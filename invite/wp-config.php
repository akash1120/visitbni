<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bni_invite' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'elzm^dN~+#0knmdZZN0mP:>KY<FQuC3>[)a3s(A1h!lQo1-=H2Vgnvr;xw.ESzn4' );
define( 'SECURE_AUTH_KEY',  '5,gV(.OV.R,CMJ9]38Tc{?c|Sk7]O*<_D{8+fxKaTV~H#{k=c8O{{agSq@H.&8?k' );
define( 'LOGGED_IN_KEY',    'VH,xum6FMoN;ImL&Hb%ZD`G)L2VpZN{12ql0oj3oujfz7Go*>!I(_zyQT;mihHHM' );
define( 'NONCE_KEY',        'TgTjfOXV@*n/vq[_.Eio<2j(e -(nPI/mCBZ6g_+eER6ajx_2ZIn;`qI8@-Zg76?' );
define( 'AUTH_SALT',        'CrosceBp<tBGbvpkiu.3$c_K4FbLg}3ox~roV~TxRdSsr2Fz9@x*#/3Mu17#]s_P' );
define( 'SECURE_AUTH_SALT', '.lgb4.G*!kd(7)d](5$RI0ms#zq@3x$4/EQ/~06R;*Cq-p)1U1d1t+|k !Z<yI$U' );
define( 'LOGGED_IN_SALT',   'u!14[&o<z,gG~E(!MGE[5idNE$?vO4&cOG$*G@mGR/><PqT-#e#scNaukAN{DI$Y' );
define( 'NONCE_SALT',       '(gk90 &i0)6890|8h8he;g+}>5;_O8WuHAdDHvMK[#Qj[MK:5*8Y$zUhXHNC]sa[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
