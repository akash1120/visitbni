<div style="width: 80%;;margin:10px auto;padding: 50px;border: 1px solid #ddd;">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <tr>
            <td>
                <meta>
                <meta>
                <div>
                    <div><p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif;">Dear <?= $email['name']; ?>
                                , </span>
                        </p>
 
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; ">We’re happy to confirm your attendance at the </span>
                        </p>

                        <p style="text-align: center"><strong>The Big Business Breakfast </strong></p>
                        <br>
                        <img src="http://visitbni.xyz/img/email.jpg" style="width: 98%">
                        <br>

                        <p><strong>Check-in starts       -   6:30 am </strong></p>
                        <p><strong>Open Networking  -   6:30 am to 7:00 am </strong></p>
                        <p><strong>Meeting Starts        -   7:00 am </strong></p>
                        <br>
                        <p> <strong>Day/Date - Tuesday, March 3rd, 2020 </strong></p>
                        <br>
                        <p> <strong> Venue      - Hilton Dubai Jumeirah</strong></p>

                            <p class="MsoNormal"><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; "><a
                                            href="https://g.page/HiltonDubaiJumeirah?share"
                                            target="_blank">Google Map Location</a> </span></p>
             <br>
                        <h4>We will then network over breakfast.</h4>
                        <ul>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; "> We charge a meeting fee of AED. 110 per head, that includes the buffet Breakfast (after) the meeting&nbsp;</span>
                            </li>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif;"> Parking is offered for Free, so you don’t have to worry about that</span>
                            </li>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; ">Please feel free to bring along a business partner, colleague or friend if you think they would benefit from the meeting</span>
                            </li>
                        </ul>
                        <p class="MsoNormal"><u><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; ">Please remember to bring plenty of business cards (a minimum of 100) to pass around, as you will meet many local business professionals.</span></u>
                        </p>
                        <p class="MsoNormal">&nbsp;</p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; ">Looking forward to welcoming you to the</span><span
                                    style="font-family: &quot;Arial&quot;, sans-serif;">&nbsp;<b><i>“World of Networking Success”</i></b>...&nbsp;</span>
                        </p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif;">&nbsp;</span>
                        </p>
                        <p class="MsoNormal"><span style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;</span></p>

                        <p class="MsoNormal" style="line-height: 12.75pt"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Business category:</span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif"><?= $email['category']; ?></span>
                        </p>
                        <p class="MsoNormal" style="line-height: 12.75pt"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Company: </span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif"><?= $email['company']; ?> </span>
                        </p>
                        <p class="MsoNormal"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Mobile:</span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;<?= $email['phone']; ?></span>
                        </p>

                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>