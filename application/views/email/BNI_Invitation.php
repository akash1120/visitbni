<div style="width: 80%;;margin:10px auto;padding: 50px;border: 1px solid #ddd;">
    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <tr>
            <td>
                <meta>
                <meta>
                <div>
                    <div><p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Dear <?= $email['name']; ?>
                                , </span>
                        </p>
 
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Further to our conversation and your expression of interest, I’m happy to invite you to attend the BNI meeting in <?= $chapterInfo->chap_location ?>  </span>
                        </p>
                        <?php if(isset($chapterInfo->chap_location_map) && ($chapterInfo->chap_location_map <> null)){ ?>
                            <p class="MsoNormal"><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)"><a
                                            href="<?= $chapterInfo->chap_location_map; ?>"
                                            target="_blank">Google Map Location</a> </span></p>
                        <?php } ?>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-size: 16pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(79, 122, 40)">The <?= $chapterInfo->chap_name ;?> Chapter</span></b>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><span
                                    style="font-size: 16pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(5, 42, 89)">will meet at</span>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">6.30 am prompt until 8.30 am&nbsp;on</span></b>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(79, 122, 40)"><?= date('l, F jS, Y', strtotime($email['date'])); ?></span></b>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(79, 122, 40)">at</span></b>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(79, 122, 40)"><?= $chapterInfo->chap_location; ?> </span></b>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">We will then&nbsp;network over breakfast.</span></b>
                        </p>
                        <p class="MsoNormal" align="center" style="text-align: center"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">&nbsp;</span></b>
                        </p>

                        <p class="MsoNormal"><span style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;</span></p>
                        <ul>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">We charge a meeting fee of AED. <?= $chapterInfo->chap_meeting_fee; ?> per head, that includes a full buffet Breakfast <u>after</u> the meeting. &nbsp;</span>
                            </li>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Parking is offered for Free, so you don’t have to worry about that.</span>
                            </li>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Please let us know if you will be able to attend as soon as possible, so that we can reserve a seat for you.</span>
                            </li>
                            <li class="MsoNormal" style=""><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Also feel free to bring along a business partner, colleague or friend if you think they would benefit from meeting successful business owners at BNI.</span>
                            </li>
                        </ul>
                        <p class="MsoNormal"><u><span
                                        style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Please remember to bring plenty of business cards (a minimum of 50) to pass around, as you will meet many local business professionals.</span></u>
                        </p>
                        <p class="MsoNormal">&nbsp;</p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Looking forward to welcoming you to the</span><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(79, 122, 40)">&nbsp;<b><i>“World of Networking Success”</i></b>...&nbsp;</span>
                        </p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(79, 122, 40)">&nbsp;</span>
                        </p>
                        <p class="MsoNormal"><span style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;</span></p>
                        <p class="MsoNormal"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Name of visitor:</span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;<?= $email['name']; ?></span>
                        </p>
                        <p class="MsoNormal" style="line-height: 12.75pt"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Business category:</span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif"><?= $CategoryName; ?></span>
                        </p>
                        <p class="MsoNormal" style="line-height: 12.75pt"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Company: </span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;<?= $email['company']; ?></span>
                        </p>
                        <p class="MsoNormal"><b><span
                                        style="font-family: &quot;Arial&quot;, sans-serif">Mobile:</span></b><span
                                    style="font-family: &quot;Arial&quot;, sans-serif">&nbsp;<?= $email['phone']; ?></span>
                        </p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">&nbsp;</span>
                        </p>
                        <p class="MsoNormal">&nbsp;</p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">Best regards,</span>
                        </p>
                        <p class="MsoNormal"><span
                                    style="font-family: &quot;Arial&quot;, sans-serif; color: rgb(20, 55, 107)">&nbsp;</span>
                        </p>
                        <p class="MsoNormal"><b><span
                                        style="font-size: 13pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(0, 32, 96)"><?= $adminInfo->name; ?></span></b><br/>
                            <?php
                            if ($adminInfo->designation <> null) {
                                ?>
                                <b><span
                                            style="font-size: 10pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(0, 32, 96)"><?= $adminInfo->designation; ?></span></b>
                                <br/>
                                <?php
                            }
                            ?>
                            <b><span
                                        style="font-size: 10pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(0, 32, 96)"><?= $adminInfo->usercompany; ?></span></b><br/>
                            <span lang="FR"
                                  style="font-size: 10pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(0, 32, 96)"><a
                                        href="mailto:<?= $adminInfo->email; ?>" target="_blank"><span
                                            style="color: rgb(5, 99, 193)"><?= $adminInfo->email; ?></span></a> <b>&nbsp;</b></span><br/>
                            <b><span lang="FR"
                                     style="font-size: 10pt; font-family: &quot;Arial&quot;, sans-serif; color: rgb(0, 32, 96)"><?= $adminInfo->mobile; ?>
                                    (mobile)</span></b>
                        </p></div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>