<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="<?php echo base_url(); ?>dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                </a>
            </li>
            <?php
            if ($role == ROLE_ADMIN || $role == ROLE_CHAPTER_ADMIN) {
            ?>
            <li class="treeview">
                <a href="/chapter/chaptersListing">
                    <i class="fa fa-file"></i>
                    <span>Chapters</span>
                </a>
            </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-file"></i>
                        <span>Chapter Admin</span>
                        <span class="pull-right-container">
                             <i class="fa fa-angle-down pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu" style="">

                        <li>
                            <a href="/chapter/chaptersAdminsList">
                                <i class="fa fa-users"></i>
                                <span>Chapter Admins</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>userListing">
                                <i class="fa fa-users"></i>
                                <span>Members</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php }
            ?>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Visitor Host</span>
                    <span class="pull-right-container">
                             <i class="fa fa-angle-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="">

                    <li>
                        <a href="/user/view_invitations">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            View Invitations
                        </a>
                    </li>
                    <?php
                    if ($role == ROLE_ADMIN || $role == ROLE_CHAPTER_ADMIN) {
                        ?>
                        <li class="treeview">
                            <a href="<?php echo base_url(); ?>user/weekly_closure">
                                <i class="fa fa-list-ul"></i>
                                <span>Weekly Closure</span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-ul"></i>
                    <span>New Membership</span>
                    <span class="pull-right-container">
                             <i class="fa fa-angle-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="">
                    <?php
                    if ($role == ROLE_ADMIN || $role == ROLE_CHAPTER_ADMIN) {
                        ?>
                        <li class="treeview">
                            <a href="<?php echo base_url(); ?>user/weekly_closure">
                                <i class="fa fa-list-ul"></i>
                                <span>Weekly Closure</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?php echo base_url(); ?>user/app_status">
                                <i class="fa fa-list-ul"></i>
                                <span>Application Status</span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span> Secretary Treasurer</span>
                    <span class="pull-right-container">
                             <i class="fa fa-angle-down pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="">
                    <?php
                    if ($role == ROLE_ADMIN || $role == ROLE_CHAPTER_ADMIN) {
                        ?>


                        <li class="treeview">
                            <a href="<?php echo base_url(); ?>user/weekly_fee">
                                <i class="fa fa-list-ul"></i>
                                <span>Weekly Fee Member</span>
                            </a>
                        </li>

                        <li class="treeview">
                            <a href="<?php echo base_url(); ?>user/weekly_receipts">
                                <i class="fa fa-list-ul"></i>
                                <span>Weekly Receipt Member</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?php echo base_url(); ?>user/weekly_visitor_receipts">
                                <i class="fa fa-list-ul"></i>
                                <span>Weekly Receipt Visitor</span>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>



        </ul>
    </section>
    <!-- /.sidebar -->
</aside>