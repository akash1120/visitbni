<?php
$loggedin = $this->session->userdata('home_logged_in');

?>
<nav class="navbar navbar-default theme-background theme-border" style="border-top-left-radius: 0; border-top-right-radius: 0;">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only color-white">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand color-white" href="/">BNI Energizers</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/home/invite_members" class="color-white">Invite Member</a></li>
                <!-- <a href="/home/view_invitations">View Invitations</a></li> -->
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <?php
                if ($loggedin['isLoggedinFront']) {
                    ?>
                    <li><a href="/home/logout" class="color-white">Logout</a></li>
                    <?php
                } else {
                    ?>
                    <li><a href="/home/login" class="color-white">Login</a></li>
                    <?php
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>