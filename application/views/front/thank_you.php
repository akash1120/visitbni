<!DOCTYPE html>
<html>
<head>
    <title>Invitation Sent</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.3/datepicker.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.3/datepicker.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">
</head>
<body>
<?php
$this->load->view('front/nav'); ?>

<div class="container">
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 loginbox">
        <div class="form_error">
            <?php
            if($this->session->flashdata('success_message')){
                ?>
                <div class="alert-success alert">
                   <h3> Your Invitation has been sent. Thank You!</h3>
                </div>
            <?php
            }
            ?>

            <?php
            if($this->session->flashdata('error_message')){
                ?>
                <div class="alert-danger alert">
                    <h3> Error! Your Invitation could not be sent. Try agaian!</h3>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script>

    $(function () {
        $('#datetimepicker1').datepicker({
            format: 'YYYY/MM/DD'
        });
    });
</script>