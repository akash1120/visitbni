<!DOCTYPE html>
<html>
<head>
    <title>Invite Members</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.3/datepicker.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.3/datepicker.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<?php
 ?>

<div class="container">
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 loginbox">
        <div class="form_error">
            <?php
            if ($validation_errors) {
                ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="panel panel-info" style="border-color:#fddcb4;">
            <div class="panel-heading" style="background:#fddcb4; border-color:#fddcb4; color: #c77005">
                <div class="panel-title"> Fill up form information</div>
            </div>

            <div class="panel-body panel-pad">
                <?php
                if ($this->session->flashdata('success_message')) {
                    ?>
                    <div id="login-alert"
                         class="alert alert-success col-sm-12 login-alert"><?= $this->session->flashdata('success_message') ?></div>
                    <?php
                }
                ?>

                <?php
                if ($this->session->flashdata('error_message')) {
                    ?>
                    <div id="login-alert"
                         class="alert alert-danger col-sm-12 login-alert"><?= $this->session->flashdata('error_message') ?></div>
                    <?php
                }
                ?>
                <!--->
                <form id="loginform" method="post" action="/home/invite_members_site" class="form-horizontal" role="form">

                    <div class="input-group margT25">
                        <label class="radio-inline">
                            <input type="radio" name="type" value="visitor" checked aria-selected="true">Visitor
                        </label>
                    </div>
                    <div class="input-group margT25">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-user"></i>
							</span>
                        <input id="login-username" type="text" required class="form-control" name="name" value=""
                               placeholder="Name" style="z-index: 1">
                    </div>
                    <div class="input-group margT25">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-envelope"></i>
							</span>
                        <input id="login-username" type="email" required class="form-control" name="email" value=""
                               placeholder="Email">
                    </div>
                    <div class="input-group margT25">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                        <input id="login-password" type="text" required class="form-control" name="phone" placeholder="Phone">
                    </div>
                    <div class="input-group margT25">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-list-alt"></i>
							</span>
                        <input id="login-username" type="text" class="form-control" required name="company" value=""
                               placeholder="Company">
                    </div>

                    <div class="input-group margT25" style="width: 100%;">
<span class="input-group-addon">
								<i class="glyphicon glyphicon-list"></i>
							</span>

                        <input id="category" type="text" class="form-control" required name="category" value=""
                               placeholder="Business Category">


                    </div>
                    <div class="input-group margT25">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-user"></i>
							</span>
                        <input id="inv_invitedBy_text" type="text" class="form-control" required name="inv_invitedBy_text" value=""
                               placeholder="Invited By">
                    </div>

                    <div id="add_category"></div>

                    <div class="input-group margT25" style="width: 100%;z-index: 1;">
                        <input  type="text" class="form-control"  readonly value="3rd March, 2020"
                               placeholder="date">
                    </div> 

                    <div class="form-group margT10">
                        <!-- Button -->
                        <div class="col-sm-12 controls">
                            <input type="submit" id="btn-login" href="#" style="background:#F99D29; border-color:#F99D29;" class="btn btn-block btn-success"
                                   value="Register">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script>


    $(document).ready(function () {

        var html = "";

        html    +=  '<div class="input-group margT25">';
        html    +=  '   <span class="input-group-addon">';
        html    +=  '       <i class="glyphicon glyphicon-list"></i>';
        html    +=  '   </span>';
        html    +=  '   <input id="login-username" type="text" class="form-control" name="new_category" required  value="" placeholder="Category">';
        html    +=  '</div>';

        $(function () {
            $('#datetimepicker1').datepicker({
                format: 'YYYY/MM/DD'
            });
        });

        $("#category").change(function () {

            if($(this).val() == "add_new"){
                $("#add_category").html(html);
            } else{
                $("#add_category").html("");
            }

        });
    });


</script>