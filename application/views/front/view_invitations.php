<!DOCTYPE html>
<html>
<head>
    <title>Invite Members</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">
</head>
<body>
<?php
$this->load->view('front/nav'); ?>

<div class="container">

    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title"><h4>Invitations</h4></div>
        </div>
        <div class="panel-body panel-pad">
            <table id="example" class="table table-striped table-bordered table-condensed dt-responsive" cellspacing="0"
                   width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Company</th>
                    <th>Category</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Invited By</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($invitations) > 0) {
                    foreach ($invitations as $invt) {
                        ?>
                        <tr>
                            <td><?= $invt->inv_name; ?></td>
                            <td><?= $invt->inv_email; ?></td>
                            <td><?= $invt->inv_phone; ?></td>
                            <td><?= $invt->inv_compnay; ?></td>
                            <td><?= $invt->cat_name; ?></td>
                            <td><?= $invt->inv_date; ?></td>
                            <td><?= $invt->inv_type; ?></td>
                            <td><?= $invt->name; ?></td>
                            <td class="text-center" id="statusSign-<?= $invt->inv_id; ?>">
                                <?php
                                if (!$invt->inv_status) {
                                    ?>
                                    <span class="label label-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </span>
                                    <?php
                                } else {
                                    ?>
                                    <span class="label label-success ">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </span>
                                    <?php
                                }
                                ?>

                            </td>
                            <td id="statusButton-<?=$invt->inv_id;?>">
                                <?php
                                if (!$invt->inv_status) {
                                    ?>
                                    <a href="#" class="label label-success change_status"
                                       id="1-<?= $invt->inv_id; ?>">confirm</a>
                                    <?php
                                } else {
                                    ?>
                                    <a href="#" class="label label-danger change_status"
                                       id="0-<?= $invt->inv_id; ?>">Cancel</a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</body>
</html>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {

        $('#example').DataTable();

        $(document).on('click', '.change_status', function(e) {
        // $('.change_status').click(function (e) {

            e.preventDefault();

            var status = $(this).attr('id');
            status = status.split('-');

            $.ajax({
                url: '/home/changeStatus',
                type: 'post',
                data: {"status": status[0], "id": status[1]},
                success: function (data, textStatus, jQxhr) {
                    if(data == 'confirmed'){
                        $("#statusButton-"+status[1]).html('<a href="#" class="label label-danger change_status" id="0-'+status[1]+'">Cancel</a>');
                        $("#statusSign-"+status[1]).html('<span class="label label-success "><span class="glyphicon glyphicon-ok"></span></span>');
                    }else {
                        $("#statusButton-"+status[1]).html('<a href="#" class="label label-success change_status" id="1-'+status[1]+'">Confirm</a>');
                        $("#statusSign-"+status[1]).html('<span class="label label-danger "><span class="glyphicon glyphicon-remove"></span></span>');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(jqXhr);
                }
            });

        });
    });
</script>