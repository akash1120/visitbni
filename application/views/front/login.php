<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <title>Login Template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Select2 CSS -->

    <link href="<?= base_url() ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Select2 JS -->
    <script src="<?= base_url() ?>assets/plugins/select2/dist/js/select2.min.js"></script>
<style>


    .select2-selection{
        height: 32px !important;
    }


</style>

</head>
<body>




<div class="container">


    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 loginbox">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>

        <div class="panel panel-info theme-border">
            <div class="panel-heading theme-background-light theme-color theme-border">
                <div class="panel-title"> Sign In</div>
            </div>
            <div class="panel-body panel-pad">

                <!--<div id="login-alert" class="alert alert-danger col-sm-12 login-alert"></div>-->
                <form id="loginform" method="post" action="/home/login" class="form-horizontal" role="form">

                    <div class="input-group margT25" style="width: 100%;">
                        <select name="chapter" class=" form-control select-2" required="required" data-show-subtext="true"
                                data-live-search="true" id="chapter_selected">
                            <option value="">Select Chapter</option>
                            <?php
                            if (count($chapters) > 0) {
                                foreach ($chapters as $chapter) {
                                    ?>
                                    <option value="<?= $chapter->chap_id; ?>"><?= $chapter->chap_name; ?></option>
                                <?php }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="input-group margT25" style="width: 100%;">
                        <select name="UserId" class="selectpicker form-control select-2" required="required" data-show-subtext="true"
                                data-live-search="true" id="admin_selected">
                            <option value="">Select User</option>
                        </select>
                    </div>
                    <div class="input-group margT25">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="text" class="form-control" required="required" name="phone" placeholder="Password">
                    </div>

                    <div class="form-group margT10">
                        <!-- Button -->
                        <div class="col-sm-12 controls">
                            <input type="submit" id="btn-login" href="#" class="btn btn-block btn-success theme-background "
                                   value="Login">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    $(document).ready(function () {

        $("#chapter_selected").change(function () {

            var id = $(this).val();

            $.ajax({
                url: "<?php echo base_url();?>home/getChapterAdmins",
                type: "POST",
                data: {"chapter" : id},
                success: function (data, textStatus, jqXHR) {
                    $("#admin_selected").html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });

        });
        $('.select-2').select2();
    });
</script>