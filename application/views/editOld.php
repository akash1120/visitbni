<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';
$company = '';
$category = '';
$designation = '';
if (!empty($userInfo)) {
    foreach ($userInfo as $uf) {
        $userId = $uf->userId;
        $name = $uf->name;
        $email = $uf->email;
        $mobile = $uf->mobile;
        $roleId = $uf->roleId;
        $company = $uf->usercompany;
        $category = $uf->usercategory;
        $designation = $uf->designation;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Member Management
            <small>Add / Edit User</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->


                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name"
                                               name="fname" value="<?php echo $name; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId"/>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter email"
                                               name="email" value="<?php echo $email; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password"
                                               name="password" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword"
                                               placeholder="Confirm Password" name="cpassword" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control" id="mobile" placeholder="Mobile Number"
                                               name="mobile" value="<?php echo $mobile; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Company</label>
                                        <input type="text" class="form-control required"
                                               name="company" value="<?php echo $company; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Designation</label>
                                        <input type="text" class="form-control " name="designation"
                                               value="<?= $designation; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Category</label>
                                        <select class="form-control required selectpicker form-control"
                                                data-show-subtext="true" data-live-search="true" id="category"
                                                name="category">
                                            <option value="">Select Category</option>
                                            <option value="add_new">Add new Category</option>
                                            <?php
                                            if (count($categories) > 0) {
                                                foreach ($categories as $cat) {
                                                    ?>
                                                    <option value="<?= $cat->cat_id ?>" <?php if ($cat->cat_id == $category) {
                                                        echo "selected=selected";
                                                    } ?>> <?= $cat->cat_name . " - " . $cat->cat_code; ?></option>
                                                <?php }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="newCategory"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if (!empty($roles)) {
                                                foreach ($roles as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->roleId; ?>" <?php if ($rl->roleId == $roleId) {
                                                        echo "selected=selected";
                                                    } ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit"/>
                            <input type="reset" class="btn btn-default" value="Reset"/>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    var html = "";

    html    +=  '<div class="col-md-6 col-xs-12 col-sm-12 col-lg-6">';
    html    +=  '   <div class="form-group">';
    html    +=  '       <label>New Category</label>';
    html    +=  '       <input  type="text" class="form-control" name="new_category" required  value="" placeholder="Category">';
    html    +=  '   </div>';
    html    +=  '</div>';

    $("#category").change(function () {

        if($(this).val() == "add_new"){
            $("#newCategory").html(html);
        } else{
            $("#newCategory").html("");
        }

    });
</script>
<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
