<style>

    .order-card {
        color: #fff;
        text-align: left;
    }

    .bg-c-blue {
        background: linear-gradient(45deg, #4099ff, #73b4ff);
    }

    .bg-c-green {
        background: linear-gradient(45deg, #2ed8b6, #59e0c5);
    }

    .bg-c-yellow {
        background: linear-gradient(45deg, #FFB64D, #ffcb80);
    }

    .bg-c-pink {
        background: linear-gradient(45deg, #FF5370, #ff869a);
    }

    .bg-c-darkblue {
        background: linear-gradient(45deg, #8101cb, #d080fe);
    }

    .bg-c-theme {
        background: linear-gradient(45deg, #8f244b, #d65c89);
    }

    .card {
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 2.94px 0.06px rgba(4, 26, 55, 0.16);
        box-shadow: 0 1px 2.94px 0.06px rgba(4, 26, 55, 0.16);
        border: none;
        margin-bottom: 30px;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    .card .card-block {
        padding: 25px;
    }

    .order-card i {
        font-size: 26px;
    }

    .f-left {
        float: left;
    }

    .f-right {
        float: right;
    }
</style>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="content-wrapper">
    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Invitations Summary</h4></div>
                    </div>
                    <div style="margin-top: 20px;">
                        <form id="form" action="" method="post">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>From</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker"
                                               name="from_date" value="<?= $from_date; ?>"
                                               id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>To</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker" name="to_date"
                                               id="datepicker" value="<?= $to_date; ?>">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-2" style="padding: 23px;">
                                <input type="submit" class="btn btn-primary" value="Filter"/>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body panel-pad table-responsive">


                        <div class="row">
                            <a href="<?= base_url('/chapter/invitation_test?from_date='.$from_date.'&to_date='.$to_date); ?>">
                            <div class="col-md-4 col-xl-3">
                                <div class="card bg-c-blue order-card">
                                    <div class="card-block">
                                        <h3 class="m-b-20">Invited</h3>
                                        <h2 class="text-right"><i
                                                    class="fa fa-users f-left"></i><span><?= $dashboard_invitations['invited'] ?></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <a href="<?= base_url('/user/view_invitations?from_date='.$from_date.'&to_date='.$to_date.'&visited=1'); ?>">
                            <div class="col-md-4 col-xl-3">
                                <div class="card bg-c-green order-card">
                                    <div class="card-block">
                                        <h3 class="m-b-20">Visited</h3>
                                        <h2 class="text-right"><i
                                                    class="fa fa-user-plus f-left"></i><span><?= $dashboard_invitations['visited'] ?></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <a href="<?= base_url('/user/app_status?selection_status=all&from_date='.$from_date.'&to_date='.$to_date); ?>">
                            <div class="col-md-4 col-xl-3">
                                <div class="card bg-c-yellow order-card">
                                    <div class="card-block">
                                        <h3 class="m-b-20">Form Filled</h3>
                                        <h2 class="text-right"><i
                                                    class="fa fa-list f-left"></i><span><?= $dashboard_invitations['form_filled'] ?></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <a href="<?= base_url('/user/app_status?selection_status=6&from_date='.$from_date.'&to_date='.$to_date); ?>">
                            <div class="col-md-4 col-xl-3">
                                <div class="card bg-c-pink order-card">
                                    <div class="card-block">
                                        <h3 class="m-b-20">Awaiting Interview</h3>
                                        <h2 class="text-right"><i
                                                    class="fa fa-newspaper-o f-left"></i><span><?= $dashboard_invitations['interviewed'] ?></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <a href="<?= base_url('/user/app_status?selection_status=3&from_date='.$from_date.'&to_date='.$to_date); ?>">
                            <div class="col-md-4 col-xl-3">
                                <div class="card bg-c-darkblue order-card">
                                    <div class="card-block">
                                        <h3 class="m-b-20">Selected</h3>
                                        <h2 class="text-right"><i
                                                    class="fa fa-thumbs-up f-left"></i><span><?= $dashboard_invitations['selected'] ?></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <a href="<?= base_url('/user/app_status?selection_status=2&from_date='.$from_date.'&to_date='.$to_date); ?>">
                            <div class="col-md-4 col-xl-3">
                                <div class="card bg-c-theme order-card">
                                    <div class="card-block">
                                        <h3 class="m-b-20">Rejected</h3>
                                        <h2 class="text-right"><i
                                                    class="fa fa-times-circle-o f-left"></i><span><?= $dashboard_invitations['rejected'] ?></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>

                    </div>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        $('.datepicker').datepicker({
            autoclose: true,
            // format: 'yyyy-mm-dd'
        });
    });

</script>