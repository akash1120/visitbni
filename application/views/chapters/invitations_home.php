
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>



<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<style>
    div.dt-buttons{
        padding-left: 20px;
        top:-5px;
    }
</style>
<div class="content-wrapper">
    <section class="content">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>user/add_invitation"><i
                                class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Invitations</h4></div>
                    </div>
                    <div style="margin-top: 20px;">
                        <form id="form" action="" method="get">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>From</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker"
                                               name="from_date" value="<?= dbToSysDate($from_date) ?>"
                                               id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>To</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker" name="to_date"
                                               value="<?= dbToSysDate($to_date); ?>" id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 23px;">
                                <input type="submit" class="btn btn-primary" value="Filter"/>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body panel-pad table-responsive">

        <table class="table table-bordered" id="posts">
            <thead>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Company</th>
            <th>Category</th>
            <th>Date</th>
            <th>Invited By</th>
            <th>Type</th>
            <th>Called</th>
            <th>Confirmed</th>
            <th>Actions</th>
            </thead>
        </table>
                    </div>
                </div>
    </section>
</div>

<script>
    $(document).ready(function () {

        $('.datepicker').datepicker({
            autoclose: true
        });



        $('#posts').DataTable({
            "processing": true,
            "paging": true,
            "serverSide": true,
            dom: 'lBrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
            ],
           /* 'pageLength': 15,*/
            "ajax":{
                "url": "<?php echo base_url('chapter/posts') ?>",
                "dataType": "json",
                "type": "POST",
                "data":{from_date: '<?= $from_date ?>', to_date: '<?= $to_date ?>', visited : <?= $visited ?> }
            },
            "columns": [
                { "data": "inv_name" },
                { "data": "inv_email" },
                { "data": "inv_phone" },
                { "data": "inv_compnay" },
                { "data": "cat_name" },
                { "data": "inv_date" },
                { "data": "name" },
                { "data": "inv_type" },
                { "data": "inv_called" },
                { "data": "inv_status" },
                { "data": "action" },
            ]

        });
    });
</script>