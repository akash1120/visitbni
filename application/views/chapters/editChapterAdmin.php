<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
<link href="<?= base_url() ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<!-- Select2 JS -->
<script src="<?= base_url() ?>assets/plugins/select2/dist/js/select2.min.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Chapter Admins
            <small>Add / Edit Chapters</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <?php
                if ($this->session->flashdata('success')) {
                    echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
                }

                if ($this->session->flashdata('error')) {

                    echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
                }
                ?>
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Assign Admins</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" id="addUser" action="<?php echo base_url() ?>chapter/updateChapterAdmin" method="post"
                          role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Chapter</label>
                                        <select class="form-control select-2 required" id="chapter" name="chapter">
                                            <option value="">Select Chapter</option>
                                            <?php
                                            if (count($chapters) > 0) {
                                                foreach ($chapters as $record) {
                                                    $selected = "";
                                                    if($ChapterAdmin <> null && $ChapterAdmin->ch_chap_id == $record->chap_id){
                                                        $selected = "selected";
                                                    }
                                                    ?>
                                                    <option value="<?= $record->chap_id; ?>"><?= $record->chap_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" name="chapter_admin_id" value="<?= $ChapterAdmin->ch_ad_id; ?>" />
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">User</label>
                                        <select class="form-control select-2 required" id="role" name="user">
                                            <option  value="">Select User</option>
                                            <?php
                                            if (count($users) > 0) {
                                                foreach ($users as $record) {
                                                    $selected = "";
                                                    if($ChapterAdmin <> null && $ChapterAdmin->ch_admin_id == $record->userId){
                                                        $selected = "selected";
                                                    }
                                                    ?>
                                                    <option value="<?= $record->userId; ?>"><?= $record->name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="addDates"></div>
                                <div class="col-lg-4 pull-right" style="font-size: 24px;">
                                    <span class="label-success label" id="addNewDate">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit"/>
                            <input type="reset" class="btn btn-default" value="Reset"/>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {

        // $(document).on('focus', '.datepicker', function(e) {

        /* $(".datepicker").datetimepicker({
             format: 'YYYY/MM/DD'
         });*/
        //});


        $('body').on('click', 'datepicker', function () {
            $(this).datepicker('destroy').datetimepicker({showOn: 'focus'}).focus();
        });


        var html = '';
        html += '<div class="col-lg-12" style="margin-top:10px;">';
        html += '   <div class="col-lg-8">';
        html += '       <div class="input-group margT25">';
        html += '           <span class="input-group-addon">';
        html += '            <i class="glyphicon glyphicon-calendar"></i>';
        html += '           </span>';
        html += '           <input id="datetimepicker1" type="text" class="form-control datepicker" name="chapter_date[]" value="" placeholder="Date">';
        html += '       </div>';
        html += '   </div>';
        html += '   <div class="col-lg-4">';
        html += '       <span class="label-danger label remove_chapter_date"><i class="fa fa-minus-circle" aria-hidden="true"></i><span>';
        html += '   </div>';
        html += '</div>';

        $("#addNewDate").click(function (e) {
            e.preventDefault();
            $("#addDates").append(html);
        })


        $(document).on('click', '.remove_chapter_date', function (e) {
            e.preventDefault();
            $(this).parent('div').parent('div').remove();
        });

    });
</script>
