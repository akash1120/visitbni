<?php
$invitationStatus = array('0' => 'No', '1' => 'Yes', '2' => 'Not Picking', '3' => 'Invite Again');
?>
<div class="content-wrapper">
    <section class="content">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>
        <div class="row">

            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Application Status</h4></div>
                    </div>

                    <div style="margin-top: 20px;">
                        <form id="form" action="" method="get">
                            <div class="col-md-4" style="padding: 23px;">
                                <div class="input-group margT25" style="width: 100%;">
                                    <label>Selected Status </label>
                                    <select name="selection_status" class="form-control select-2"
                                            id="">
                                        <option value="all" <?php if ($selection_status == 'all') {
                                            echo 'selected';
                                        } ?>>Select Status
                                        </option>
                                        <option value="0" <?php if ($selection_status == '0') {
                                            echo 'selected';
                                        } ?>>Awaiting Interview
                                        </option>
                                      <!--  <option value="1" <?php /*if ($selection_status == '1') {
                                            echo 'selected';
                                        } */?>>Interview Done
                                        </option>-->
                                        <option value="2" <?php if ($selection_status == '2') {
                                            echo 'selected';
                                        } ?>>Rejected
                                        </option>
                                        <option value="3" <?php if ($selection_status == '3') {
                                            echo 'selected';
                                        } ?>>Inducted
                                        </option>
                                        <option value="4" <?php if ($selection_status == '4') {
                                            echo 'selected';
                                        } ?>>Not Available
                                        </option>
                                        <option value="5" <?php if ($selection_status == '5') {
                                            echo 'selected';
                                        } ?>>Awaiting Voting
                                        </option>
                                        <option value="6" <?php if ($selection_status == '6') {
                                            echo 'selected';
                                        } ?>>Awaiting Documents
                                        </option>
                                        <option value="7" <?php if ($selection_status == '7') {
                                            echo 'selected';
                                        } ?>>Awaiting Induction
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4" style="padding: 48px;">

                                <input type="submit" class="btn btn-primary" value="Filter"/>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>


                    <div class="panel-body panel-pad table-responsive">
                        <table id="example"
                               class="display table"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <!-- <th>Company</th>-->
                                <th>Category</th>
                                <th>Rating</th>
                                <th>Form Filling Date</th>

                                <th>Interview Date</th>

                                <th>Status</th>

                                <th>Comments</th>
                                <th>Update</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($invitations) > 0) {
                                foreach ($invitations as $invt) {
                                    ?>
                                    <tr>
                                        <td><?= $invt->inv_name; ?>
                                            <br>
                                            <b>Company: </b> <?= $invt->inv_compnay; ?>
                                            <br>
                                            <b>Phone: </b> <?= $invt->inv_phone; ?>
                                            <br>
                                            <b>Invited By: </b> <?= ($invt->invited_by_name <> null)? $invt->invited_by_name : '-'; ?>
                                        </td>
                                        <td><?= ($invt->cat_name <> null) ? $invt->cat_name : '-'; ?></td>


                                        <td>
                                            <span style="display: none;"><?php echo $invt->inv_rating; ?></span>

                                            <?php if ($invt->inv_rating == 0) {
                                                ?>
                                                <span class="fa fa-star-o "></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            <?php } else if ($invt->inv_rating == 1) { ?>
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            <?php } else if ($invt->inv_rating == 2) { ?>
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star-o"></span>
                                            <?php } else if ($invt->inv_rating == 3) { ?>
                                                <span class="fa fa-star "></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            <?php } ?>

                                        </td>
                                        <td>
                                            <?php if ($invt->inv_date <> null && $invt->inv_date != "0000-00-00") {
                                                echo $invt->inv_date;
                                            } ?>

                                        </td>
                                        <td>
                                            <?php if ($invt->inv_interview_date <> null && $invt->inv_interview_date != "0000-00-00") {
                                                echo $invt->inv_interview_date;
                                            } ?>
                                            <?php if ($invt->interview1 <> null) { ?>
                                                <br>
                                                <b>Interviewed By</b> : <?= $invt->interview1; ?>
                                            <?php } ?>
                                            <?php if ($invt->interview2 <> null) { ?>
                                                ,<br> <?= $invt->interview2; ?>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ($invt->inv_selection_status == "2") {
                                                echo 'Rejected';
                                            } else if ($invt->inv_selection_status == "3") {
                                                echo 'Inducted';
                                            } else if ($invt->inv_selection_status == "4") {
                                                echo 'Not Available';
                                            } else if ($invt->inv_selection_status == "5") {
                                                echo 'Awaiting Voting';
                                            } else if ($invt->inv_selection_status == "6") {
                                                echo 'Awaiting Documents';
                                            } else if ($invt->inv_selection_status == "7") {
                                                echo 'Awaiting Induction';
                                            } else {
                                                echo 'Awaiting Interview';
                                            }
                                            ?>

                                        </td>

                                        <td><?= $invt->inv_comments; ?></td>
                                        <td>
                                            <a data_id="<?= $invt->inv_id; ?>"
                                               data_form="<?= $invt->inv_form_filled; ?>"
                                               data_category="<?= $invt->inv_category ?>"
                                               data_rating="<?= $invt->inv_rating ?>"
                                               data_comments="<?= $invt->inv_comments; ?>"
                                               data_documents="<?= $invt->documents; ?>"
                                               data_inter1="<?= $invt->inv_interviewedBy1; ?>"
                                               data_inter2="<?= $invt->inv_interviewedBy2; ?>"
                                               data_date="<?= ($invt->inv_interview_date <> null && $invt->inv_interview_date != "0000-00-00") ? $invt->inv_interview_date : date('Y-m-d'); ?>"
                                               data_select="<?= $invt->inv_selection_status; ?>"
                                               class="btn btn-primary btn-sm edit_weekly_row"
                                               style="cursor: pointer;">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
    </section>
</div>


<div id="updateWeeklyInv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Status</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/user/update_app_status " method="post">
                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" name="inv_id" id="inv_weekly_id">
                        <input type="hidden" name="current_url" value="<?= ($_GET['selection_status'])? $_GET['selection_status']: '' ; ?>">

                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Interviewer 1</label>
                                <select name="inv_interviewedBy1" class="form-control select-2"
                                        id="interviewer1">
                                    <option value="">Select Interviewer 1</option>
                                    <?php
                                    if ($users <> null) {
                                        foreach ($users as $user) {
                                            $selected = "";
                                            if ($invt->inv_interviewedBy1 == $user->userId) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Interviewer 2</label>
                                <select name="inv_interviewedBy2" class="form-control select-2"
                                        id="inv_interviewer2">
                                    <option value="">Select Interviewer 2</option>
                                    <?php
                                    if ($users <> null) {
                                        foreach ($users as $user) {
                                            $selected = "";
                                            if ($invt->inv_interviewedBy2 == $user->userId) {
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Interview Date</label>
                                <input type="text"
                                       class="form-control "
                                       name="inv_interview_date"
                                       id="datepicker_app"
                                >
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="inv_selection_status" class="formFilledStatus form-control select-2"
                                        id="inv_selection_app">
                                    <option value="0">Awaiting Interview</option>
                                    <option value="2">Rejected</option>
                                    <option value="3">Inducted</option>
                                    <option value="4">Not Available</option>
                                    <option value="5">Awaiting Voting</option>
                                    <option value="6">Awaiting Documents</option>
                                    <option value="7">Awaiting Induction</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-6 col-xs-12 col-sm-12 ">
                            <div class="form-group">
                                <label>Category</label>
                                <select name="inv_category" class="form-control select-2" required="required"
                                        id="category">
                                    <option value="">Select Category</option>
                                    <option value="add_new">Add new Category</option>
                                    <?php
                                    if ($categories <> null) {
                                        foreach ($categories as $category) {
                                            ?>
                                            <option value="<?= $category->cat_id; ?>"><?= $category->cat_name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div id="newCategory"></div>

                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Rating</label>
                                <select name="inv_rating" class="form-control select-2"
                                        id="inv_rating_weekly">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <label>Documents Collected</label><br>
                            <input type="checkbox" name="docs[]" id="licence" value="licence">Licence<br>
                            <input type="checkbox" name="docs[]" id="trn" value="trn">TRN<br>

                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12" style="padding-top: 5px;">
                            <label></label>
                            <br>
                            <input type="checkbox" name="docs[]" id="eid" value="eid">EID<br>
                            <input type="checkbox" name="docs[]" id="cheque" value="cheque">Cheque<br>

                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Comments</label>
                                <textarea id="inv_comments" name="inv_comments" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2 pull-right">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary"
                                           value="Save"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- </div>-->

            </div>

        </div>

    </div>
</div>


<script>
    $(document).ready(function () {

        $('.datepicker').datepicker({
            autoclose: true
        });

        var table = $('#example').DataTable({

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                }
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            pageLength: 30,
            responsive: false,
            /*
             "bAutoWidth": false,
             "columnDefs": [
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             ]
             */

        });

        /*
         new $.fn.dataTable.Responsive(table, {
         details: false
         });
         */


    });

    $('#addWeeklyInv').on('shown.bs.modal', function (e) {


    })


    $(".edit_weekly_row").on('click', function () {


        var id = $(this).attr('data_id');
        var data_form = $(this).attr('data_form');
        var data_category = $(this).attr('data_category');
        var data_rating = $(this).attr('data_rating');
        var data_comments = $(this).attr('data_comments');
        var data_documents = $(this).attr('data_documents');
        var data_inter1 = $(this).attr('data_inter1');
        var data_inter2 = $(this).attr('data_inter2');
        var data_date = $(this).attr('data_date');
        var data_select = $(this).attr('data_select');
        if (data_documents !== '') {
            var arr = data_documents.split(',');
            if (jQuery.inArray("licence", arr) !== -1) {
                $('#licence').prop('checked', true);
            }
            if (jQuery.inArray("eid", arr) !== -1) {
                $('#eid').prop('checked', true);
            }
            if (jQuery.inArray("trn", arr) !== -1) {
                $('#trn').prop('checked', true);
            }
            if (jQuery.inArray("cheque", arr) !== -1) {
                $('#cheque').prop('checked', true);
            }
            console.log(arr);
        }


        $('#inv_weekly_id').val('');
        $('#inv_weekly_id').val(id);
        $('#inv_formfilled_weekly').val('');
        $('#inv_formfilled_weekly').val(data_form).trigger('change');

        $('#interviewer1');
        $('#interviewer1').val(data_inter1).trigger('change');

        $('#interviewer2').val('');
        $('#inv_interviewer2').val(data_inter2).trigger('change');

        $('#category').val('');
        $('#category').val(data_category).trigger('change');
        $('#inv_rating_weekly').val('');
        $('#inv_rating_weekly').val(data_rating).trigger('change');

        $('#inv_selection_app').val('');
        $('#inv_selection_app').val(data_select).trigger('change');

        $('#inv_comments').val('');
        $('#inv_comments').val(data_comments);
        if((data_date != "") && (data_date != "0000-00-00")) {
            $('#datepicker_app').val(data_date);
        }
        alert(data_date);
     //   $('#datepicker_app').val(data_date);

        $('#datepicker_app').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#updateWeeklyInv').modal('show');
    });


</script>