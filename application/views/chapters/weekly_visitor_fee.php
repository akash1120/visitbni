<?php
$invitationStatus = array('0' => 'No', '1' => 'Yes', '2' => 'Not Picking', '3' => 'Invite Again');
?>
<style>
    .add_btn_weekly {
        background-image: none !important;
        background-color: #3C8DBC !important;
        color: white !important;
        font-size: 14px;
    }

</style>

<div class="content-wrapper">
    <section class="content">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-right" style="padding-bottom: 20px">
                <div class="form-group">
                    <!--<a class="btn btn-primary" href="<?php /*echo base_url(); */ ?>user/add_invitation"><i
                                class="fa fa-plus"></i> Add New</a>-->
                    <!--  <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                              data-target="#addWeeklyInv">
                          <i class="fa fa-plus"></i> Add New
                      </button>-->
                </div>
            </div>
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Weekly Visitor Fee</h4></div>
                    </div>

                    <div style="margin-top: 20px;">

                        <form id="form" action="" method="get">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right"
                                               name="from_date"
                                               value="<?= $next_meeting ?>"
                                               id="chapter_date_holiday">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4" style="padding: 23px;">
                                <input type="submit" class="btn btn-primary" value="Filter"/>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>


                    <div class="panel-body panel-pad table-responsive">
                        <table id="example"
                               class="display table"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Fee</th>
                                <th>Descrition</th>
                                <th>Update</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($weeklyfees) > 0) {
                                foreach ($weeklyfees as $invt) {
                                    /*  echo "<pre>";
                                      print_r($invt->inv_form_filled);
                                      die;*/
                                    ?>
                                    <tr>
                                        <td><?= $invt->inv_name; ?></td>

                                        <td><?= gridDate($invt->week_date); ?></td>
                                        <td><?= $invt->amount; ?></td>
                                        <td><?= $invt->description; ?></td>

                                        <td>
                                            <a data_id="<?= $invt->id; ?>"
                                               data_comments="<?= $invt->description; ?>"
                                               data_amount="<?= $invt->amount; ?>"
                                               data_date="<?= ($invt->week_date <> null) ? dbToSysDate($invt->week_date) : date('m/d/Y'); ?>"
                                               data_user="<?= $invt->user_id; ?>"
                                               class="btn btn-primary btn-sm edit_weekly_row"
                                               style="cursor: pointer;">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href="<?= base_url(); ?>user/delete_weekly_visitor_fee?id=<?= $invt->id; ?>&del_date=<?= dbToSysDate($invt->week_date); ?>"
                                            <a href="<?= base_url(); ?>user/delete_weekly_visitor_fee?id=<?= $invt->id; ?>&del_date=<?= dbToSysDate($invt->week_date); ?>"
                                               class="btn btn-danger btn-sm delete_inv"
                                               id="<?= $invt->inv_id; ?>">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
    </section>
</div>


<!-- start update Modal -->
<div id="addWeeklyInv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Fee</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/user/add_weekly_visitor_fee" method="post">

                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" name="chapter_id" value="<?= $chapter_id; ?>">

                        <div class="col-md-6 col-xs-12 col-sm-6 ">
                            <div class="form-group">
                                <label>Visitor</label>
                                <select name="user_id" class="form-control select-2"
                                        id="user_id" required>
                                    <option value="">Select Visitor</option>
                                    <?php
                                    if ($users <> null) {
                                        foreach ($users as $user) {
                                            $selected = "";
                                            ?>
                                            <option value="<?= $user->inv_id; ?>" <?= $selected; ?> ><?= $user->inv_name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-6 ">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text"
                                       class="form-control"
                                       name="week_date" autocomplete="off"
                                       id="add_week_fee_date" required="required"
                                >
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-6 ">
                            <div class="form-group">
                                <label>Fee Amount</label>
                                <input type="number" class="form-control required" name="amount"
                                       placeholder="Enter Fee Amount"
                                       value="<?= $this->session->userdata('chapter')->chap_meeting_fee ?>"
                                       required="required">
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id="inv_comments" name="description" class="form-control"></textarea>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-2 pull-right">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary"
                                           value="Save"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <!-- </div>-->

            </div>
        </div>

    </div>
</div>
<!--end update Modal-->


<div id="updateWeeklyInv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Fee</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/user/update_weekly_visitor_fee" method="post">
                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" name="id" id="inv_weekly_id">
                        <input type="hidden" name="from_date"
                               value="<?= ($_GET['from_date']) ? $_GET['from_date'] : ''; ?>">

                        <div class="col-md-6 col-xs-12 col-sm-6 ">
                            <div class="form-group">
                                <label>Visitor</label>
                                <select name="user_id" class="form-control select-2"
                                        id="user_id_update" required>
                                    <option value="">Select Visitor</option>
                                    <?php
                                    if ($users <> null) {
                                        foreach ($users as $user) {
                                            $selected = "";

                                            ?>
                                            <option value="<?= $user->inv_id; ?>" <?= $selected; ?> ><?= $user->inv_name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-6 ">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text"
                                       class="form-control"
                                       name="week_date" autocomplete="off"
                                       id="add_week_fee_date_update"
                                >
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-6 ">
                            <div class="form-group">
                                <label>Fee Amount</label>
                                <input type="number" id="weekly_fee_update" class="form-control required" name="amount"
                                       placeholder="Enter Fee Amount" required="required">
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id="inv_comments_update" name="description" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2 pull-right">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary"
                                           value="Save"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <!-- </div>-->

            </div>
        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true
        });

        $('#chapter_date_holiday').datepicker({
            //format: "yyyy-mm-dd",
            // startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted: "<?= $chapter_info->event_day; ?>"

        });

        var table = $('#example').DataTable({

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                },
                {
                    text: '<i class="fa fa-plus"></i> Add New',
                    className: "add_btn_weekly",
                    action: function (e, dt, node, config) {
                        $('#addWeeklyInv').modal('show');
                    }
                }
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            pageLength: 30,
            responsive: false,
            /*
             "bAutoWidth": false,
             "columnDefs": [
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             ]
             */

        });

        /*
         new $.fn.dataTable.Responsive(table, {
         details: false
         });
         */


    });

    $('#addWeeklyInv').on('shown.bs.modal', function (e) {

        $('#datepicker_weekly').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#add_week_fee_date').datepicker({
            //format: "yyyy-mm-dd",
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted: "<?= $chapter_info->event_day; ?>"

        });

    })


    $(".edit_weekly_row").on('click', function () {

        var id = $(this).attr('data_id');
        var data_form = $(this).attr('data_form');
        var data_form = $(this).attr('data_form');
        var data_date = $(this).attr('data_date');
        var data_comments = $(this).attr('data_comments');
        var data_user = $(this).attr('data_user');
        var data_amount = $(this).attr('data_amount');

        $('#inv_weekly_id').val('');
        $('#inv_weekly_id').val(id);

        $('#weekly_fee_update').val('');
        $('#weekly_fee_update').val(data_amount);
        $('#add_week_fee_date_update').val('');
        $('#add_week_fee_date_update').val(data_date);

        $('#user_id_update').val('');
        $('#user_id_update').val(data_user).trigger('change');

        $('#inv_comments_update').val('');
        $('#inv_comments_update').val(data_comments);


        $('#updateWeeklyInv').modal('show');
    });
    $('#updateWeeklyInv').on('shown.bs.modal', function (e) {

        $('#add_week_fee_date_update').datepicker({
            //format: "yyyy-mm-dd",
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted: "<?= $chapter_info->event_day; ?>"

        });

    })

</script>