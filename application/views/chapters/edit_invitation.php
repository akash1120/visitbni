<link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="container">
                    <?php
                    if ($this->session->flashdata('success')) {
                        echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
                    }

                    if ($this->session->flashdata('error')) {

                        echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
                    }
                    ?>
                    <form action="/user/edit_invitation/<?= $invt_id; ?>" method="post">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title"><h4>Update Information</h4></div>
                            </div>
                            <div class="panel-body panel-pad">
                                <div class="box-body table-responsive no-padding">
                                    <?php
                                    if ($invt <> null) {

                                        ?>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label for="fname">Full Name</label>
                                                <input type="text" class="form-control required" name="inv_name"
                                                       placeholder="Enter Name" required="required"
                                                       value="<?= $invt->inv_name; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control required" name="inv_email"
                                                       placeholder="Enter Email" required="required"
                                                       value="<?= $invt->inv_email; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input type="text" class="form-control required" name="inv_phone"
                                                       placeholder="Enter Phone Number" required="required"
                                                       value="<?= $invt->inv_phone; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <input type="text" class="form-control required" name="inv_company"
                                                       placeholder="Enter Company Name" required="required"
                                                       value="<?= $invt->inv_compnay; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <select name="inv_category" class="form-control select-2"
                                                        id="category">
                                                    <option value="">Select Category</option>
                                                    <option value="add_new">Add new Category</option>
                                                    <?php
                                                    if ($categories <> null) {
                                                        foreach ($categories as $category) {
                                                            $selected = "";
                                                            if ($category->cat_id == $invt->inv_category) {
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                            <option <?= $selected; ?>
                                                                    value="<?= $category->cat_id; ?>"><?= $category->cat_name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="newCategory"></div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input type="text"
                                                       class="form-control datepicker interviewDate"
                                                       name="inv_date"
                                                       id="datepicker"
                                                       value="<?= $invt->inv_date; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Invited By</label>
                                                <select name="inv_invitedBy" class="form-control select-2">
                                                    <option value="">Select User</option>
                                                    <?php
                                                    if ($users <> null) {
                                                        foreach ($users as $user) {
                                                            $selected = "";
                                                            if ($invt->inv_invitedBy == $user->userId) {
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                            <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Type</label>
                                                <select name="inv_type" class="ChangeTypes form-control select-2">
                                                    <option value="visitor" <?= ($invt->inv_type == "visitor") ? "selected" : ""; ?> >
                                                        Visitor
                                                    </option>
                                                    <option value="guest" <?= ($invt->inv_type == "guest") ? "selected" : ""; ?> >
                                                        Guest
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Is Called?</label>
                                                <select name="inv_isCalled" class="isCalled form-control select-2">
                                                    <option value="0" <?= ($invt->inv_called == "0") ? "selected" : ""; ?> >
                                                        No
                                                    </option>
                                                    <option value="1" <?= ($invt->inv_called == "1") ? "selected" : ""; ?>>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Is Confirmed?</label>
                                                <select name="inv_confirmation" class="ChangeStatus form-control select-2">
                                                    <option value="0" <?= ($invt->inv_status == "0") ? "selected" : ""; ?> >
                                                        No
                                                    </option>
                                                    <option value="1" <?= ($invt->inv_status == "1") ? "selected" : ""; ?>>
                                                        Yes
                                                    </option>
                                                    <option value="2" <?= ($invt->inv_status == "2") ? "selected" : ""; ?>>
                                                        Not Picking
                                                    </option>
                                                    <option value="3" <?= ($invt->inv_status == "3") ? "selected" : ""; ?>>
                                                        Invite Again
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Form Filled?</label>
                                                <select name="inv_formfilled" class="formFilledStatus form-control select-2"
                                                        id="">
                                                    <option value="0" <?= ($invt->inv_form_filled == "0") ? "selected" : ""; ?> >
                                                        No
                                                    </option>
                                                    <option value="1" <?= ($invt->inv_form_filled == "1") ? "selected" : ""; ?>>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Visited?</label>
                                                <select name="visited" class="isVisited form-control select-2">
                                                    <option value="0" <?= ($invt->inv_visited == "0") ? "selected" : ""; ?> >
                                                        No
                                                    </option>
                                                    <option value="1" <?= ($invt->inv_visited == "1") ? "selected" : ""; ?>>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Interview Date</label>
                                                <input type="text"
                                                       class="form-control datepicker  interviewDate"
                                                       name="inv_interview_date"
                                                       id="datepicker"
                                                       value="<?= $invt->inv_interview_date; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Interviewer 1</label>
                                                <select name="inv_interviewer1" class="form-control select-2"
                                                        id="interviewer1">
                                                    <option value="">Select Interviewer 1</option>
                                                    <?php
                                                    if ($users <> null) {
                                                        foreach ($users as $user) {
                                                            $selected = "";
                                                            if ($invt->inv_interviewedBy1 == $user->userId) {
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                            <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Interviewer 2</label>
                                                <select name="inv_interviewer2" class="form-control select-2"
                                                        id="inv_interviewer2">
                                                    <option value="">Select Interviewer 2</option>
                                                    <?php
                                                    if ($users <> null) {
                                                        foreach ($users as $user) {
                                                            $selected = "";
                                                            if ($invt->inv_interviewedBy2 == $user->userId) {
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                            <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Voting Done?</label>
                                                <select name="inv_voting" class="form-control select-2" id="VotingStatus ">
                                                    <option value="0" <?= ($invt->inv_voting_done == "0") ? "selected" : ""; ?> >
                                                        No
                                                    </option>
                                                    <option value="1" <?= ($invt->inv_voting_done == "1") ? "selected" : ""; ?>>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Selected?</label>
                                                <select name="inv_selection" class="form-control select-2"
                                                        id="SelectedStatus">
                                                    <option value="0" <?= ($invt->inv_selection_status == "0") ? "selected" : ""; ?> >
                                                        No
                                                    </option>
                                                    <option value="1" <?= ($invt->inv_selection_status == "1") ? "selected" : ""; ?>>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                      <!--  <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Invited By Text</label>
                                                <input type="text" class="form-control required" name="inv_invitedBy_text"
                                                       placeholder="EnterInvited By Text"
                                                       value="<?/*= $invt->inv_invitedBy_text; */?>">
                                            </div>
                                        </div>-->
                                        <div class="col-md-12">
                                            <div class="col-md-2 pull-right">
                                                <div class="form-group">
                                                    <input type="submit" class="form-control btn btn-primary"
                                                           value="Save"/>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title"><h4>Notes</h4></div>
                        </div>
                        <div class="panel-body panel-pad">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <textarea cols="5" rows="5" style="margin: 0px;width: 60%;height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        setTimeout(function () {
            $('.msg_div').hide();
        }, 5000);

        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        var html = "";

        html += '<div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">';
        html += '   <div class="form-group">';
        html += '       <label>New Category</label>';
        html += '       <input id="login-username" type="text" class="form-control" name="new_category" required  value="" placeholder="Category">';
        html += '   </div>';
        html += '</div>';

        $("#category").change(function () {

            if ($(this).val() == "add_new") {
                $("#newCategory").html(html);
            } else {
                $("#newCategory").html("");
            }

        });

    });

</script>