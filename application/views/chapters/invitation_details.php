<link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="container">
                    <?php
                    if ($this->session->flashdata('success')) {
                        echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
                    }

                    if ($this->session->flashdata('error')) {

                        echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
                    }
                    ?>
                    <form action="/user/update_invitation/<?= $invt->inv_id; ?>" method="post">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title"><h4>Invitations</h4></div>
                            </div>
                            <div class="panel-body panel-pad">
                                <?php
                                if ($invt <> null) {
                                    ?>
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover" id="userTable">
                                            <tbody>
                                            <tr>
                                                <td><strong>Name</td></strong>
                                                <td>
                                                    <?= $invt->inv_name; ?>
                                                    <input type="hidden" name="invitationId" id="invitationId"
                                                           value="<?= $invt->inv_id ?>" />
                                                </td>
                                                <td><strong>Email</td></strong>
                                                <td><?= $invt->inv_email; ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Mobile</td></strong>
                                                <td><?= $invt->inv_phone; ?></td>
                                                <td><strong>Company</td></strong>
                                                <td><?= $invt->inv_compnay; ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Category</td></strong>
                                                <td><?= $invt->cat_name; ?></td>
                                                <td><strong>Date</td></strong>
                                                <td><?= $invt->inv_date; ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Invited By</td></strong>
                                                <td><?= $invt->name; ?></td>
                                                <td><strong>Type</td></strong>
                                                <td>
                                                    <select name="type" class="ChangeTypes select-2" id="type<?= $invt->inv_id; ?>">
                                                        <option value="visitor" <?php if ($invt->inv_type == "visitor") {
                                                            echo "selected";
                                                        } ?>>Visitor
                                                        </option>
                                                        <option value="guest" <?php if ($invt->inv_type == "guest") {
                                                            echo "selected";
                                                        } ?>>Guest
                                                        </option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <td><strong>Called</td></strong>
                                            <td>
                                                <select name="isCalled" class="isCalled select-2" id="isCalled<?= $invt->inv_id; ?>">

                                                    <option value="0" <?php if ($invt->inv_called == "0") {
                                                        echo "selected";
                                                    } ?>>No
                                                    </option>
                                                    <option value="1" <?php if ($invt->inv_called == "1") {
                                                        echo "selected";
                                                    } ?>>Yes
                                                    </option>
                                                </select>
                                            </td>
                                            <td><strong>Confirmed</td></strong>
                                            <td id="statusButton-<?= $invt->inv_id; ?>">
                                                <select name="confirmation" class="ChangeStatus select-2" id="confirmation<?= $invt->inv_id; ?>">

                                                    <option value="0" <?php if ($invt->inv_status == "0") {
                                                        echo "selected";
                                                    } ?>>No
                                                    </option>
                                                    <option value="1" <?php if ($invt->inv_status == "1") {
                                                        echo "selected";
                                                    } ?>>Yes
                                                    </option>
                                                </select>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Form Filled</td></strong>
                                                <td id="statusButton-<?= $invt->inv_id; ?>">

                                                    <select name="formfilled" class="formFilledStatus select-2"
                                                            id="formfilled<?= $invt->inv_id; ?>">

                                                        <option value="0" <?php if ($invt->inv_form_filled == "0") {
                                                            echo "selected";
                                                        } ?>>No
                                                        </option>
                                                        <option value="1" <?php if ($invt->inv_form_filled == "1") {
                                                            echo "selected";
                                                        } ?>>Yes
                                                        </option>
                                                    </select>

                                                </td>
                                                <td><strong>Visited</td></strong>
                                                <td>
                                                    <select name="visited" class="isVisited select-2" id="visited<?= $invt->inv_id; ?>">

                                                        <option value="0" <?php if ($invt->inv_visited == "0") {
                                                            echo "selected";
                                                        } ?>>No
                                                        </option>
                                                        <option value="1" <?php if ($invt->inv_visited == "1") {
                                                            echo "selected";
                                                        } ?>>Yes
                                                        </option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Interview Date</td></strong>
                                                <td>
                                                    <?php
                                                    $date = date('Y-m-d');
                                                    if (isset($invt->inv_interview_date) &&  $invt->inv_interview_date <> "0000-00-00") {
                                                        $date = $invt->inv_interview_date;
                                                    }
                                                    ?>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="input-group date">
                                                                <input type="text"
                                                                       class="form-control pull-right interviewDate"
                                                                       name="interview_date"
                                                                       id="datepicker"
                                                                       value="<?= $date; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><strong>Interviewed By</td></strong>
                                                <td>
                                                    <select name="interviewer1" class="formFilledStatus select-2"
                                                            id="interviewer1">
                                                        <option value="">Select Interviewer 1</option>
                                                        <?php
                                                        if ($users <> null) {
                                                            foreach ($users as $user) {
                                                                $selected = "";
                                                                if ($invt->inv_interviewedBy1 == $user->userId) {
                                                                    $selected = "selected";
                                                                }
                                                                ?>
                                                                <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <select name="interviewer2" class="formFilledStatus select-2" id="interviewer2">
                                                        <option value="">Select Interviewer 2</option>
                                                        <?php
                                                        if ($users <> null) {
                                                            foreach ($users as $user) {
                                                                $selected = "";
                                                                if ($invt->inv_interviewedBy2 == $user->userId) {
                                                                    $selected = "selected";
                                                                }
                                                                ?>
                                                                <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Voting Done</td></strong>
                                                <td>
                                                    <select name="voting" class="formFilledStatus select-2" id="VotingStatus">

                                                        <option value="0" <?php if ($invt->inv_voting_done == "0") {
                                                            echo "selected";
                                                        } ?>>No
                                                        </option>
                                                        <option value="1" <?php if ($invt->inv_voting_done == "1") {
                                                            echo "selected";
                                                        } ?>>Yes
                                                        </option>
                                                    </select>
                                                </td>
                                                <td><strong>Selected</td></strong>
                                                <td>
                                                    <select name="selection" class="formFilledStatus select-2" id="SelectedStatus">
                                                        <option value="0" <?php if ($invt->inv_selection_status == "0") {
                                                            echo "selected";
                                                        } ?>>No
                                                        </option>
                                                        <option value="1" <?php if ($invt->inv_selection_status == "1") {
                                                            echo "selected";
                                                        } ?>>Yes
                                                        </option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th colspan="4">
                                                    <div class="col-md-2 pull-right">
                                                        <div class="form-group">
                                                            <input type="submit" class="form-control btn btn-primary"
                                                                   value="Save"/>
                                                        </div>
                                                    </div>
                                                </td></strong>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php
                                } else {
                                    echo '<span class="alert alert-danger">No Record found!</span>';
                                }
                                ?>
                            </div>
                        </div>
                    </form>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title"><h4>Notes</h4></div>
                        </div>
                        <div class="panel-body panel-pad">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <textarea cols="5" rows="5" style="margin: 0px;width: 60%;height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        setTimeout(function () {
            $('.msg_div').hide();
        }, 5000);

        $('#datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd"
        });

    });

</script>