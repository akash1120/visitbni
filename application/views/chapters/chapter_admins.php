<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Chapter Admins
            <small>Add, Edit, Delete</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>chapter/addChapterAdmin"><i class="fa fa-plus"></i>
                        Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body no-padding">
                        <table class="table table-responsive table-bordered" id="chapters_table">
                            <tr>
                                <th>Name</th>
                                <th>Admin</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            <?php
                            if (!empty($chapterAdmins)) {
                                foreach ($chapterAdmins as $record) {
                                    ?>
                                    <tr>
                                        <td><?php echo $record->chap_name ?></td>
                                        <td><?php echo $record->name ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-info"
                                               href="<?php echo base_url() . 'chapter/editChapterAdmin/' . $record->ch_ad_id; ?>"><i
                                                    class="fa fa-pencil"></i></a>
                                            <a class="btn btn-sm btn-danger deleteChapter" href="#"
                                               id="<?php echo $record->ch_ad_id; ?>"
                                               data-userid="<?php echo $record->ch_ad_id; ?>"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>

                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script>


    $(document).ready(function () {



        $("a.deleteChapter").click(function (e) {
            if (!confirm('Are you sure?')) {
                e.preventDefault();
                return false;
            } else {
                var id = $(this).attr('id');
                $.ajax({
                    url: '<?= base_url();?>chapter/deleteChapterAdmin',
                    type: 'POST',
                    data: {'id': id},
                    success: function (data) {
                        location.reload(true);
                    },
                    error: function (e) {
                        location.reload(true);
                    }
                });
            }
        });

        // $('#chapters_table').DataTable();
    });


</script>