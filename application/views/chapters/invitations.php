<?php
$invitationStatus = array('0' => 'No', '1' => 'Yes', '2' => 'Not Picking', '3' => 'Invite Again');
?>
<div class="content-wrapper">
    <section class="content">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>user/add_invitation"><i
                                class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Invitations</h4></div>
                    </div>
                    <div style="margin-top: 20px;">
                        <form id="form" action="" method="get">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>From</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker"
                                               name="from_date"
                                               id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>To</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker" name="to_date"
                                               id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 23px;">
                                <input type="submit" class="btn btn-primary" value="Filter"/>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body panel-pad table-responsive">
                        <table id="example"
                               class="display table"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Company</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Invited By</th>
                                <th>Type</th>
                                <th>Called</th>
                                <th>Confirmed</th>
                                <th class="hidden">Type</th>
                                <th class="hidden">Called</th>
                                <th class="hidden">Confirmed</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($invitations) > 0) {
                                foreach ($invitations as $invt) {
                                    ?>
                                    <tr>
                                        <td><?= $invt->inv_name; ?></td>
                                        <td><?= $invt->inv_email; ?></td>
                                        <td><?= $invt->inv_phone; ?></td>
                                        <td><?= $invt->inv_compnay; ?></td>
                                        <!--<td><?/*= ($invt->cat_name <> null)? $invt->cat_name : $invt->inv_category_site; */?></td>-->
                                        <td><?= ($invt->cat_name <> null)? $invt->cat_name : '-'; ?></td>
                                        <td><?= $invt->inv_date; ?></td>
                                        <!--<td><?/*= ($invt->name <> null)? $invt->name : $invt->inv_invitedBy_text; */?></td>-->
                                        <td><?= ($invt->name <> null)? $invt->name : '-' ?></td>
                                        <td>
                                            <div class="table_select">
                                                <select class="table_select ChangeTypes" id="<?= $invt->inv_id; ?>">
                                                    <option value="guest" <?php if ($invt->inv_type == "guest") {
                                                        echo "selected";
                                                    } ?> >Guest
                                                    </option>
                                                    <option value="visitor" <?php if ($invt->inv_type == "visitor") {
                                                        echo "selected";
                                                    } ?>>Visitor
                                                    </option>
                                                </select>
                                            </div>

                                        </td>

                                        <td>
                                            <div class="table_select">
                                                <select class="table_select isCalled" id="<?= $invt->inv_id; ?>">
                                                    <option value="0" <?php if ($invt->inv_called == "0") {
                                                        echo "selected";
                                                    } ?> >No
                                                    </option>
                                                    <option value="1" <?php if ($invt->inv_called == "1") {
                                                        echo "selected";
                                                    } ?>>Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </td>
                                        <td id="statusButton-<?= $invt->inv_id; ?>">
                                            <div class="table_select">
                                                <select class=" ChangeStatus" id="<?= $invt->inv_id; ?>">
                                                    <option value="0" <?php if ($invt->inv_status == "0") {
                                                        echo "selected";
                                                    } ?> >No
                                                    </option>
                                                    <option value="1" <?php if ($invt->inv_status == "1") {
                                                        echo "selected";
                                                    } ?>>Yes
                                                    </option>
                                                    <option value="2" <?php if ($invt->inv_status == "2") {
                                                        echo "selected";
                                                    } ?>>Not Picking
                                                    </option>
                                                    <option value="3" <?php if ($invt->inv_status == "3") {
                                                        echo "selected";
                                                    } ?>>Invite Again
                                                    </option>
                                                </select>
                                            </div>

                                        </td>
                                        <td class="hidden">
                                            <div style="text-transform:capitalize;" id="InvitationType"
                                                 class="hidden"><?= ucfirst($invt->inv_type); ?></div>
                                        </td>
                                        <td class="hidden">
                                            <div style="text-transform:capitalize;" id="isCalledText"
                                                 class="hidden"> <?= ($invitationStatus[$invt->inv_called]) ? $invitationStatus[$invt->inv_called] : "No"; ?></div>
                                        </td>
                                        <td class="hidden">
                                            <div style="text-transform:capitalize;" id="statusText"
                                                 class="hidden"> <?= ($invitationStatus[$invt->inv_status]) ? $invitationStatus[$invt->inv_status] : "No"; ?></div>
                                        </td>
                                        <td>
                                            <a target="_blank"
                                               href="<?= base_url(); ?>user/invitation_detail/<?= $invt->inv_id; ?>"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a target="_blank"
                                               href="<?= base_url(); ?>user/edit_invitation/<?= $invt->inv_id; ?>"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href="<?= base_url(); ?>user/deleteInvitation/<?= $invt->inv_id; ?>"
                                            <a href="<?= base_url(); ?>user/deleteInvitation/<?= $invt->inv_id; ?>"
                                               class="btn btn-danger btn-sm delete_inv"
                                               id="<?= $invt->inv_id; ?>">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        $('.datepicker').datepicker({
            autoclose: true
        });

        var table = $('#example').DataTable({

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 10, 11, 12]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 10, 11, 12]
                    }
                }
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            pageLength: 30,
            responsive: false,
            /*
            "bAutoWidth": false,
             "columnDefs": [
		    { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		  ]
		  */

        });

        /*
        new $.fn.dataTable.Responsive(table, {
            details: false
        });
        */

        table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));

        $(document).on('change', '.ChangeStatus', function (e) {

            e.preventDefault();

            var id = $(this).attr('id');
            var status = $(this).val();

            $.ajax({
                url: '/home/changeStatus',
                type: 'post',
                data: {"status": status, "id": id},
                success: function (data, textStatus, jQxhr) {
                    if (data) {
                        alert('Status Changed Successfully!');
                        if (status == "0") {
                            $(document).find("#statusText").text("No");
                        }
                        if (status == "1") {
                            $(document).find("#statusText").text("Yes");
                        }
                        if (status == "2") {
                            $(document).find("#statusText").text("Not Picking");
                        }
                        if (status == "3") {
                            $(document).find("#statusText").text("Invite Again");
                        }
                        location.reload();
                    } else {
                        alert('Error! Status could not be changed!');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(jqXhr);
                }
            });
        });


        $(document).on('change', '.ChangeTypes', function (e) {
            e.preventDefault();

            var type = $(this).val();
            var id = $(this).attr("id");

            $.ajax({
                url: '/home/changeType',
                type: 'post',
                data: {"type": type, "id": id},
                success: function (data, textStatus, jQxhr) {
                    if (data) {
                        alert('Type Changed Successfully!');
                        $(document).find(document).find("#InvitationType").text(type);
                    } else {
                        alert('Error! type could not be changed!');
                    }
                    location.reload();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(jqXhr);
                }
            });

        });

        $(document).on('change', '.isCalled', function (e) {

            e.preventDefault();
            var called = $(this).val();
            var id = $(this).attr("id");

            $.ajax({
                url: '/home/changeCallStatus',
                type: 'post',
                data: {"call": called, "id": id},
                success: function (data, textStatus, jQxhr) {
                    if (data) {
                        alert('Updated Status!');
                        if (called == "0") {
                            $(document).find("#isCalledText").text("No");
                        }
                        if (called == "1") {
                            $(document).find("#isCalledText").text("Yes");
                        }
                        location.reload();
                    } else {
                        alert('Error! Updateding Status!');
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(jqXhr);
                }
            });

        });

        $(document).on('change', '.isVisited', function (e) {

            e.preventDefault();

            var id = $(this).attr('id');
            var status = $(this).val();

            $.ajax({
                url: '/home/changeVisitStatus',
                type: 'post',
                data: {"status": status, "id": id},
                success: function (data, textStatus, jQxhr) {
                    if (data) {
                        alert('Status Changed Successfully!');
                    } else {
                        alert('Error! Status could not be changed!');
                    }
                    location.reload();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(jqXhr);
                }
            });

        });

        $(document).on('change', '.formFilledStatus', function (e) {

            e.preventDefault();

            var id = $(this).attr('id');
            var status = $(this).val();

            $.ajax({
                url: '/home/changeStatusFormFilled',
                type: 'post',
                data: {"status": status, "id": id},
                success: function (data, textStatus, jQxhr) {
                    if (data) {
                        alert('Status Changed Successfully!');
                    } else {
                        alert('Error! Status could not be changed!');
                    }
                    location.reload();
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log(jqXhr);
                }
            });

        });


        $("a.delete_inv").click(function (e) {
            if (!confirm('Are you sure?')) {
                e.preventDefault();
                return false;
            } else {
                return true;
            }
        });
    });

</script>