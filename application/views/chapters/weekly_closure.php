<?php
$invitationStatus = array('0' => 'No', '1' => 'Yes', '2' => 'Not Picking', '3' => 'Invite Again');
?>

<style>
    .add_btn_weekly{
        background-image: none !important;
        background-color: #3C8DBC !important;
        color: white !important;
        font-size: 14px;
    }
</style>

<div class="content-wrapper">
    <section class="content">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-right" style="padding-bottom: 20px">
                <div class="form-group">
                    <!--<a class="btn btn-primary" href="<?php /*echo base_url(); */ ?>user/add_invitation"><i
                                class="fa fa-plus"></i> Add New</a>-->
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                            data-target="#addWeeklyInv">
                        <i class="fa fa-plus"></i> Add New
                    </button>
                </div>
            </div>
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Weekly Closure</h4></div>
                    </div>

                    <div style="margin-top: 20px;">
                        <form id="form" action="" method="get">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Date</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right"
                                               name="from_date"
                                               value="<?= $next_meeting ?>"
                                               id="chapter_date_holiday">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>

                            <div class="col-md-4" style="padding: 23px;">
                                <input type="submit" class="btn btn-primary" value="Filter"/>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>



                    <div class="panel-body panel-pad table-responsive">
                        <table id="example"
                               class="display table"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Company</th>
                                <th>Category</th>
                                <th>Date</th>
                                <th>Is Visited?</th>
                                <!--<th>Form Filling</th>
                                <th>Rating</th>
                                <th>Comments</th>
                                <th>Update</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($invitations) > 0) {
                                foreach ($invitations as $invt) {
                                    /*  echo "<pre>";
                                      print_r($invt->inv_form_filled);
                                      die;*/
                                    ?>
                                    <tr>
                                        <td><?= $invt->inv_name; ?></td>
                                        <td><?= $invt->inv_email; ?></td>
                                        <td><?= $invt->inv_phone; ?></td>
                                        <td><?= $invt->inv_compnay; ?></td>
                                        <!--<td><?/*= ($invt->cat_name <> null)? $invt->cat_name : $invt->inv_category_site; */ ?></td>-->
                                        <td><?= ($invt->cat_name <> null) ? $invt->cat_name : '-'; ?></td>
                                        <td><?= gridDate($invt->inv_date); ?></td>
                                        <td>

                                            <?php if($invt->inv_visited == "1"){
                                                ?>
                                                <a data_id="<?= $invt->inv_id; ?>" data_form="<?= $invt->inv_form_filled;?>"
                                                   data_rating="<?= $invt->inv_rating ?>"
                                                   data_comments="<?= $invt->inv_comments; ?>"
                                                   data_visited="<?= $invt->inv_visited; ?>"
                                                   class="btn btn-success btn-sm edit_weekly_row"
                                                   style="cursor: pointer;">
                                                    Yes
                                                </a>

                                           <?php  }else{ ?>
                                                <a data_id="<?= $invt->inv_id; ?>" data_form="<?= $invt->inv_form_filled;?>"
                                                   data_rating="<?= $invt->inv_rating ?>"
                                                   data_comments="<?= $invt->inv_comments; ?>"
                                                   data_visited="<?= $invt->inv_visited; ?>"
                                                   class="btn btn-default btn-sm edit_weekly_row"
                                                   style="cursor: pointer;">
                                                    No
                                                </a>
                                           <?php  } ?>



                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
    </section>
</div>


<!-- start update Modal -->
<div id="addWeeklyInv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Invitations</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/user/add_invitation_weekly " method="post">

                            <div class="box-body table-responsive no-padding">
                                <input type="hidden" name="chapter_id" value="<?= $chapter_id; ?>">

                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" name="inv_name"
                                               placeholder="Enter Name" required="required">
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control required" name="inv_email"
                                               placeholder="Enter Email" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control required" name="inv_phone"
                                               placeholder="Enter Phone Number" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <input type="text" class="form-control required" name="inv_company"
                                               placeholder="Enter Company Name" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="inv_category" class="form-control select-2" required="required"
                                                id="category">
                                            <option value="">Select Category</option>
                                            <option value="add_new">Add new Category</option>
                                            <?php
                                            if ($categories <> null) {
                                                foreach ($categories as $category) {
                                                    ?>
                                                    <option value="<?= $category->cat_id; ?>"><?= $category->cat_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="newCategory"></div>
                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text"
                                               class="form-control"
                                               name="inv_date"
                                               readonly="readonly"
                                               value="<?= $next_meeting; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Invited By</label>
                                        <select name="inv_invitedBy" id="selUser" class="form-control"
                                                id="">
                                            <option value="">Select User</option>
                                            <?php
                                            if ($users <> null) {
                                                foreach ($users as $user) {
                                                    $selected = "";
                                                    if ($adminLoggedinId && $adminLoggedinId == $user->userId) {
                                                        $selected = "selected";
                                                    }
                                                    ?>
                                                    <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12 col-sm-6 ">
                                    <div class="form-group">
                                        <label>Form Filled?</label>
                                        <select name="inv_formfilled" class=" form-control select-2" id="">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <div class="form-group">
                                            <input type="submit" class="form-control btn btn-primary"
                                                   value="Save"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                </form>
                <!-- </div>-->

            </div>
        </div>

    </div>
</div>
<!--end update Modal-->



<div id="updateWeeklyInv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Invitations</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/user/update_invitation_weekly " method="post">
                            <div class="box-body table-responsive no-padding">
                                <input type="hidden" name="inv_id" id="inv_weekly_id">
                                <input type="hidden" name="current_url" value="<?= ($_GET['from_date'])? $_GET['from_date']: '' ; ?>">

                                <div class="col-sm-12" style="padding-top:10px; padding-bottom: 20px ">
                                    <label>
                                        <input type="checkbox" name="visited_check" id="visited_check" value="1"> Visited
                                    </label>
                                </div>


                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Form Filled?</label>
                                        <select name="inv_form_filled" class="formFilledStatus form-control"
                                                id="inv_formfilled_weekly">
                                            <option value="0">
                                                No
                                            </option>
                                            <option value="1">
                                                Yes
                                            </option>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Rating</label>
                                        <select name="inv_rating" class="formFilledStatus form-control"
                                                id="inv_rating_weekly">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Comments</label>
                                        <textarea id="inv_comments" name="inv_comments" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <div class="form-group">
                                            <input type="submit" class="form-control btn btn-primary"
                                                   value="Save"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                </form>
                <!-- </div>-->

            </div>
        </div>

    </div>
</div>




<script>
    $(document).ready(function () {

        $('#chapter_date_holiday').datepicker({
            //format: "yyyy-mm-dd",
            autoclose: true,
           // startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted:  "<?= $chapter_info->event_day; ?>"

        });

        var table = $('#example').DataTable({

            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
                {
                    text: '<i class="fa fa-plus"></i> Add New',
                    className: "add_btn_weekly",
                    action: function ( e, dt, node, config ) {
                        $('#addWeeklyInv').modal('show');
                    }
                }
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            pageLength: 30,
            responsive: false,
            /*
             "bAutoWidth": false,
             "columnDefs": [
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "10%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             { "sWidth": "5%", "targets": 0 },
             ]
             */

        });

        /*
         new $.fn.dataTable.Responsive(table, {
         details: false
         });
         */



    });

    $('#addWeeklyInv').on('shown.bs.modal', function (e) {

        $('#datepicker_weekly').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        $("#selUser").select2();

        $("#selUser1").select2({
            ajax: {
                url: '<?= base_url() ?>index.php/User/getUsers',
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });



    })


    $(".edit_weekly_row").on('click', function () {

        var id = $(this).attr('data_id');
        var data_form = $(this).attr('data_form');
        var data_rating = $(this).attr('data_rating');
        var data_comments = $(this).attr('data_comments');
        var data_visited = $(this).attr('data_visited');

        $('#inv_weekly_id').val('');
        $('#inv_weekly_id').val(id);
        $('#inv_formfilled_weekly').val('');
        $('#inv_formfilled_weekly').val(data_form).trigger('change');

        $('#inv_rating_weekly').val('');
        $('#inv_rating_weekly').val(data_rating).trigger('change');

        $('#inv_comments').val('');
        $('#inv_comments').val(data_comments);

       if(data_visited == 1) {
           $('#visited_check').prop('checked', true);
       }

        $('#updateWeeklyInv').modal('show');
    });


</script>