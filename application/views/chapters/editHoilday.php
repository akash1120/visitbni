<link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="container">
                    <?php
                    if ($this->session->flashdata('success')) {
                        echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
                    }

                    if ($this->session->flashdata('error')) {

                        echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
                    }
                    ?>
                    <form action="/chapter/editHoilday/<?= $holiday_id; ?>" method="post">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title"><h4>Update Information</h4></div>
                            </div>
                            <div class="panel-body panel-pad">
                                <div class="box-body table-responsive no-padding">
                                    <?php
                                    if ($holiday <> null) {

                                        ?>
                                        <input type="hidden" name="id" value="<?= $holiday_id ?>">
                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label for="fname">Name</label>
                                                <input type="text" class="form-control required" name="name"
                                                       placeholder="Enter Name" required="required"
                                                       value="<?= $holiday->name; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <input type="text"
                                                       class="form-control "
                                                       name="holiday_date"
                                                       id="chapter_date"
                                                       value="<?= dbToSysDate($holiday->holiday_date); ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-2 pull-right">
                                                <div class="form-group">
                                                    <input type="submit" class="form-control btn btn-primary"
                                                           value="Save"/>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        setTimeout(function () {
            $('.msg_div').hide();
        }, 5000);

        $('.datepicker').datepicker({
            format: "yyyy-mm-dd"
        });

    });

</script>