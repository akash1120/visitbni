
<div class="content-wrapper">
    <section class="content">
        <?php
        if ($this->session->flashdata('success')) {
            echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
        }

        if ($this->session->flashdata('error')) {

            echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
        }
        ?>
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>chapter/addHoliday"><i
                                class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>Holidays</h4></div>
                    </div>
                    
                    <div class="panel-body panel-pad table-responsive">
                        <table id="example"
                               class="display table"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (count($holidays) > 0) {
                                foreach ($holidays as $holiday) {
                                    ?>
                                    <tr>
                                        <td><?= $holiday->name; ?></td>
                                        <td><?= $holiday->holiday_date; ?></td>

                                        <td>
                                            <a target="_blank"
                                               href="<?= base_url(); ?>chapter/editHoilday/<?= $holiday->id; ?>"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href="<?= base_url(); ?>chapter/deleteHoliday/<?= $holiday->id; ?>"
                                            <a href="<?= base_url(); ?>chapter/deleteHoliday/<?= $holiday->id; ?>"
                                               class="btn btn-danger btn-sm delete_holiday"
                                               id="<?= $holiday->id; ?>">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {


        var table = $('#example').DataTable({

            dom: 'Bfrtip',
            buttons: [
              /*  {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1]
                    }
                }*/
            ],
            paging: true,
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            pageLength: 30,
            responsive: false,
            /*
            "bAutoWidth": false,
             "columnDefs": [
		    { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "10%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		     { "sWidth": "5%", "targets": 0 },
		  ]
		  */

        });

        /*
        new $.fn.dataTable.Responsive(table, {
            details: false
        });
        */

        table.buttons().container().appendTo($('.col-sm-6:eq(0)', table.table().container()));


        $("a.delete_holiday").click(function (e) {
            if (!confirm('Are you sure?')) {
                e.preventDefault();
                return false;
            } else {
                return true;
            }
        });
    });

</script>