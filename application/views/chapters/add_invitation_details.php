<link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet"
      type="text/css"/>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <?php
                if ($this->session->flashdata('success')) {
                    echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
                }

                if ($this->session->flashdata('error')) {

                    echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
                }
                ?>
                <!-- general form elements -->
                <div class="container">
                    <form action="/user/add_invitation " method="post">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title"><h4>Add Invitations</h4></div>
                            </div>
                            <div class="panel-body panel-pad">
                                <div class="box-body table-responsive no-padding">
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="fname">Full Name</label>
                                            <input type="text" class="form-control required" name="inv_name"
                                                   placeholder="Enter Name" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control required" name="inv_email"
                                                   placeholder="Enter Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" class="form-control required" name="inv_phone"
                                                   placeholder="Enter Phone Number" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Company</label>
                                            <input type="text" class="form-control required" name="inv_company"
                                                   placeholder="Enter Company Name" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select name="inv_category" class="form-control select-2" required="required"
                                                    id="category">
                                                <option value="">Select Category</option>
                                                <option value="add_new">Add new Category</option>
                                                <?php
                                                if ($categories <> null) {
                                                    foreach ($categories as $category) {
                                                        ?>
                                                        <option value="<?= $category->cat_id; ?>"><?= $category->cat_name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="newCategory"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="text"
                                                   class="form-control datepicker interviewDate"
                                                   name="inv_date"
                                                   id="datepicker"
                                                   value="<?= date('Y-m-d'); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Invited By</label>
                                            <select name="inv_invitedBy" class="form-control select-2"
                                                    id="">
                                                <option value="">Select User</option>
                                                <?php
                                                if ($users <> null) {
                                                    foreach ($users as $user) {
                                                        $selected = "";
                                                        if ($adminLoggedinId && $adminLoggedinId == $user->userId) {
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                        <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select name="inv_type" class="ChangeTypes form-control select-2">
                                                <option value="visitor">Visitor</option>
                                                <option value="guest">Guest</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Is Called?</label>
                                            <select name="inv_isCalled" class="isCalled form-control select-2">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Is Confirmed?</label>
                                            <select name="inv_confirmation" class="ChangeStatus form-control select-2">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                                <option value="2" >Not Picking</option>
                                                <option value="3" >Invite Again
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Form Filled?</label>
                                            <select name="inv_formfilled" class="formFilledStatus form-control select-2" id="">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Visited?</label>
                                            <select name="visited" class="isVisited form-control select-2">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Interview Date</label>
                                            <input type="text"
                                                   class="form-control datepicker interviewDate"
                                                   name="inv_interview_date"
                                                   id="datepicker"
                                                   value="<?= date('Y-m-d'); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Interviewer 1</label>
                                            <select name="inv_interviewer1" class="form-control select-2"
                                                    id="interviewer1">
                                                <option value="">Select Interviewer 1</option>
                                                <?php
                                                if ($users <> null) {
                                                    foreach ($users as $user) {
                                                        $selected = "";
                                                        if ($invt->inv_interviewedBy1 == $user->userId) {
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                        <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Interviewer 2</label>
                                            <select name="inv_interviewer2" class="form-control select-2"
                                                    id="inv_interviewer2">
                                                <option value="">Select Interviewer 2</option>
                                                <?php
                                                if ($users <> null) {
                                                    foreach ($users as $user) {
                                                        $selected = "";
                                                        if ($invt->inv_interviewedBy2 == $user->userId) {
                                                            $selected = "selected";
                                                        }
                                                        ?>
                                                        <option value="<?= $user->userId; ?>" <?= $selected; ?> ><?= $user->name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Voting Done?</label>
                                            <select name="inv_voting" class="form-control select-2" id="VotingStatus ">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Selected?</label>
                                            <select name="inv_selection" class="form-control select-2"
                                                    id="SelectedStatus">
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-2 pull-right">
                                            <div class="form-group">
                                                <input type="submit" class="form-control btn btn-primary"
                                                       value="Save"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title"><h4>Notes</h4></div>
                        </div>
                        <div class="panel-body panel-pad">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <textarea cols="5" rows="5" style="margin: 0px;width: 60%;height: 100px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
</div>
<script>
    $(document).ready(function () {

        setTimeout(function () {
            $('.msg_div').hide();
        }, 5000);

        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        var html = "";

        html    +=  '<div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">';
        html    +=  '   <div class="form-group">';
        html    +=  '       <label>New Category</label>';
        html    +=  '       <input id="login-username" type="text" class="form-control" name="new_category" required  value="" placeholder="Category">';
        html    +=  '   </div>';
        html    +=  '</div>';

        $("#category").change(function () {

            if($(this).val() == "add_new"){
                $("#newCategory").html(html);
            } else{
                $("#newCategory").html("");
            }

        });

    });

</script>