
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <?php
                if ($this->session->flashdata('success')) {
                    echo "<div class='alert alert-success msg_div' >" . $this->session->flashdata('success') . "</div>";
                }

                if ($this->session->flashdata('error')) {

                    echo "<div class='alert alert-danger msg_div'>" . $this->session->flashdata('error') . "</div>";
                }
                ?>
                <?php
                $tuesday = strtotime('last Tuesday');
                // check if we need to go back in time one more week
               // $tuesday = date('W', $tuesday)==date('W') ? $tuesday-7*86400 : $tuesday;
                echo getCurrentDate();
                ?>
                <!-- general form elements -->
                <div class="container">
                    <form action="/chapter/addHoliday " method="post">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title"><h4>Add Holiday</h4></div>
                            </div>
                            <div class="panel-body panel-pad">
                                <div class="box-body table-responsive no-padding">
                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="fname">Name</label>
                                            <input type="text" class="form-control required" name="name"
                                                   placeholder="Enter Name" required="required">
                                        </div>
                                    </div>


                                    <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="text"
                                                   class="form-control datepicker interviewDate"
                                                   name="holiday_date"
                                                   id="chapter_date" autocomplete="off"
                                                   >
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-2 pull-right">
                                            <div class="form-group">
                                                <input type="submit" class="form-control btn btn-primary"
                                                       value="Save"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </section>
</div>


<script>
    $(document).ready(function () {

        setTimeout(function () {
            $('.msg_div').hide();
        }, 5000);
    });

</script>