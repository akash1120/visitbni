<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">-->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
<link href="<?= base_url() ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<!-- Select2 JS -->
<script src="<?= base_url() ?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/ckeditor/ckeditor.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Chapter Management
            <small>Add / Edit Chapters</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Chapter Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" id="addUser" action="<?php echo base_url() ?>chapter/saveChapter" method="post"
                          role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Name</label>
                                        <input type="text" class="form-control required" id="name" name="name"
                                               maxlength="128" value="<?php echo $ChapterInfo->chap_name ?>">
                                        <input type="hidden" class="form-control required" id="name" name="id"
                                               maxlength="128" value="<?php echo $ChapterInfo->chap_id ?>">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Location</label>
                                        <input type="text" class="form-control required email" id="location"
                                               name="location" maxlength="128"
                                               value="<?php echo $ChapterInfo->chap_location ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Location Map</label>
                                        <input type="text" class="form-control required email" id="location_map"
                                               name="location_map" maxlength="128"
                                               value="<?php echo $ChapterInfo->chap_location_map ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Meeting Fee</label>
                                        <input type="text" class="form-control required" name="meeting_fee" maxlength=""
                                               value="<?php echo $ChapterInfo->chap_meeting_fee ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Event day</label>
                                        <select name="event_day" class="ChangeTypes form-control select-2">
                                            <option value="0" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 0)) {
                                                echo 'selected';
                                            } ?>>Sunday
                                            </option>
                                            <option value="1" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 1)) {
                                                echo 'selected';
                                            } ?>>Monday
                                            </option>
                                            <option value="2" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 2)) {
                                                echo 'selected';
                                            } ?>>Tuesday
                                            </option>
                                            <option value="3" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 3)) {
                                                echo 'selected';
                                            } ?>>Wednesday
                                            </option>
                                            <option value="4" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 4)) {
                                                echo 'selected';
                                            } ?>>Thursday
                                            </option>
                                            <option value="5" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 5)) {
                                                echo 'selected';
                                            } ?>>Friday
                                            </option>
                                            <option value="6" <?php if (($ChapterInfo->event_day <> null) && ($ChapterInfo->event_day == 6)) {
                                                echo 'selected';
                                            } ?>>Saturday
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email Template Data</label>
                                    <code> (<b>Variables</b> {visitor_name},{chapter_location},{chapter_location_map},{chapter_name},{date},{chapter_meeting_fee},{visitor_category},{visitor_company},{visitor_phone},{InvitedBy_name},{InvitedBy_company},{InvitedBy_email},{InvitedBy_mobile} )</code><br>
                             <textarea id="myeditor" name="email_template" ><?php echo $ChapterInfo->email_template ?></textarea>
                                </div>
                            </div>
                            <!--  <div class="row">
                                <div id="addDates">
                                    <?php
                            /*                                    if ($ChapterDate <> null && count($ChapterDate) > 0) {
                                                                    foreach ($ChapterDate as $dates) {
                                                                        */ ?>
                                            <div class="col-lg-12" style="margin-top:10px;">
                                                <div class="col-lg-8">
                                                    <div class="input-group margT25">
                                                        <span class="input-group-addon">
                                                            <i class="glyphicon glyphicon-calendar"></i>
                                                        </span>
                                                        <input id="datetimepicker1" type="text"
                                                               class="form-control datepicker" name="chapter_date[]"
                                                               value="<?php /*echo $dates->cahpter_date;*/ ?> " placeholder="Date">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                   <span class="label-danger label remove_chapter_date">
                                                       <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                   </span>
                                                </div>
                                            </div>
                                            <?php
                            /*                                        }
                                                                }
                                                                */ ?>
                                </div>
                                <div class="col-lg-4 pull-right" style="font-size: 24px;">
                                    <span class="label-success label" id="addNewDate">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>-->
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit"/>
                            <input type="reset" class="btn btn-default" value="Reset"/>
                        </div>
                    </form>
                </div>
            </div>

<div class="clearfix"></div>
            <div class="col-sm-12">
            <?php
            $this->load->helper('form');
            $error = $this->session->flashdata('error');
            if ($error) {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>
            <?php
            $success = $this->session->flashdata('success');
            if ($success) {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <div class="row">
                   <!-- <div class="col-xs-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-primary" href="<?php /*echo base_url(); */?>chapter/addHoliday"><i
                                        class="fa fa-plus"></i> Add New</a>
                        </div>
                    </div>-->
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="col-sm-6">
                                    <div class="panel-title">
                                        <h4>Holidays</h4>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                                            data-target="#addHoilday"><i
                                                class="fa fa-plus"></i> Add New
                                    </button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="col-md-12" style="padding-top: 20px;">

                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="panel-body panel-pad table-responsive">
                                <table id="example"
                                       class="display table"
                                       cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (count($holidays) > 0) {
                                        foreach ($holidays as $holiday) {
                                            ?>
                                            <tr>
                                                <td><?= $holiday->name; ?></td>
                                                <td><?= $holiday->holiday_date; ?></td>

                                                <td>
                                                    <a data_id="<?= $holiday->id; ?>" data_name="<?= $holiday->name; ?>"
                                                       data_date="<?= dbToSysDate($holiday->holiday_date); ?>"
                                                       data_chapter_id="<?= $holiday->chapter_id; ?>"
                                                       class="btn btn-primary btn-sm edit_holiday"
                                                       style="cursor: pointer;">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="<?= base_url(); ?>chapter/deleteHoliday/<?= $holiday->id; ?>"
                                                    <a href="<?= base_url(); ?>chapter/deleteHoliday/<?= $holiday->id; ?>"
                                                       class="btn btn-danger btn-sm delete_holiday"
                                                       id="<?= $holiday->id; ?>">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <!-- <div class="col-xs-12 text-right">
                        <div class="form-group">
                            <a class="btn btn-primary" href="<?php /*echo base_url(); */?>chapter/addHoliday"><i
                                        class="fa fa-plus"></i> Add New</a>
                        </div>
                    </div>-->
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="col-sm-6">
                                    <div class="panel-title">
                                        <h4>Free weeks</h4>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                                            data-target="#addFreeweek"><i
                                                class="fa fa-plus"></i> Add New
                                    </button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="col-md-12" style="padding-top: 20px;">


                                <div class="row">
                                    <div class="col-md-12">
                                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="panel-body panel-pad table-responsive">
                                <table id="example"
                                       class="display table"
                                       cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (count($freeweeks) > 0) {
                                        foreach ($freeweeks as $freeweek) {
                                            ?>
                                            <tr>
                                                <td><?= $freeweek->name; ?></td>
                                                <td><?= $freeweek->week_date; ?></td>

                                                <td>
                                                    <a data_id="<?= $freeweek->id; ?>" data_name="<?= $freeweek->name; ?>"
                                                       data_date="<?= dbToSysDate($freeweek->week_date); ?>"
                                                       data_chapter_id="<?= $freeweek->chapter_id; ?>"
                                                       class="btn btn-primary btn-sm edit_week"
                                                       style="cursor: pointer;">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="<?= base_url(); ?>chapter/deleteFreeweek/<?= $freeweek->id; ?>"
                                                    <a href="<?= base_url(); ?>chapter/deleteFreeweek/<?= $freeweek->id; ?>"
                                                       class="btn btn-danger btn-sm delete_week"
                                                       id="<?= $holiday->id; ?>">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>


<!-- start add Modal -->
<div id="addHoilday" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Holiday</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/chapter/addHoliday " method="post">
                   
                            <div class="box-body table-responsive no-padding">
                                <input type="hidden" name="chapter_id" value="<?= $ChapterInfo->chap_id; ?>">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="fname">Name</label>
                                        <input type="text" class="form-control required" name="name"
                                               placeholder="Enter Name" required="required">
                                    </div>
                                </div>


                                <div class="col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text"
                                               class="form-control"
                                               name="holiday_date"
                                               id="chapter_date_holiday" autocomplete="off"
                                               required="required"
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <div class="form-group">
                                            <input type="submit" class="form-control btn btn-primary"
                                                   value="Save"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                </form>
                <!-- </div>-->

            </div>

        </div>

    </div>
</div>
<!--end add Modal-->


<!-- start update Modal -->
<div id="updateHoilday" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Holiday</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/chapter/editHoilday " method="post">

                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" name="chapter_id" value="<?= $ChapterInfo->chap_id; ?>">
                        <input type="hidden" id="id_update" name="id" value="">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="fname">Name</label>
                                <input type="text" id="name_update" class="form-control required" name="name"
                                       placeholder="Enter Name" required="required">
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text"
                                       class="form-control"
                                       name="holiday_date"
                                       id="chapter_date_holiday_update" autocomplete="off"
                                >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2 pull-right">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary"
                                           value="Save"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- </div>-->

            </div>

        </div>

    </div>
</div>
<!--end update Modal-->

<!-- start add Modal -->
<div id="addFreeweek" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Free Week</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/chapter/addFreeweek " method="post">

                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" name="chapter_id" value="<?= $ChapterInfo->chap_id; ?>">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="fname">Name</label>
                                <input type="text" class="form-control required" name="name"
                                       placeholder="Enter Name" required="required">
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text"
                                       class="form-control"
                                       name="week_date"
                                       id="chapter_date_week" autocomplete="off"
                                       required="required"
                                >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2 pull-right">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary"
                                           value="Save"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <!-- </div>-->

            </div>

        </div>

    </div>
</div>
<!--end add Modal-->

<!-- start update Modal -->
<div id="updateFreeweek" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Free Week</h4>
            </div>
            <div class="modal-body">


                <!-- left column -->

                <!-- general form elements -->
                <!--<div class="container">-->
                <form action="/chapter/editFreeweek " method="post">

                    <div class="box-body table-responsive no-padding">
                        <input type="hidden" name="chapter_id" value="<?= $ChapterInfo->chap_id; ?>">
                        <input type="hidden" id="id_update_week" name="id" value="">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="fname">Name</label>
                                <input type="text" id="name_update_week" class="form-control required" name="name"
                                       placeholder="Enter Name" required="required">
                            </div>
                        </div>


                        <div class="col-md-6 col-xs-12 col-sm-12">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text"
                                       class="form-control"
                                       name="week_date"
                                       id="chapter_date_week_update" autocomplete="off"
                                >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2 pull-right">
                                <div class="form-group">
                                    <input type="submit" class="form-control btn btn-primary"
                                           value="Save"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- </div>-->

            </div>

        </div>

    </div>
</div>
<!--end update Modal-->


<script>
    $(document).ready(function () {
        console.log(<?= $holidays; ?>);

        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });


        $('body').on('click', '.datepicker', function () {
            $(this).datepicker('destroy').datepicker({showOn: 'focus'}).focus();
        });


        var html = '';
        html += '<div class="col-lg-12" style="margin-top:10px;">';
        html += '   <div class="col-lg-8">';
        html += '       <div class="input-group margT25">';
        html += '           <span class="input-group-addon">';
        html += '            <i class="glyphicon glyphicon-calendar"></i>';
        html += '           </span>';
        html += '           <input id="datetimepicker1" type="text" class="form-control datepicker" name="chapter_date[]" value="" placeholder="Date">';
        html += '       </div>';
        html += '   </div>';
        html += '   <div class="col-lg-4">';
        html += '       <span class="label-danger label remove_chapter_date"><i class="fa fa-minus-circle" aria-hidden="true"></i><span>';
        html += '   </div>';
        html += '</div>';

        $("#addNewDate").click(function (e) {
            e.preventDefault();
            $("#addDates").append(html);
        })


        $(document).on('click', '.remove_chapter_date', function (e) {
            e.preventDefault();
            $(this).parent('div').parent('div').remove();
        });


        $('#chapter_date').datepicker({
            //format: "yyyy-mm-dd",
            autoclose: true,
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted:  "<?= $ChapterInfo->event_day; ?>"

        });


    });


    $('#addFreeweek').on('shown.bs.modal', function (e) {
        $('#chapter_date_week').datepicker({
            //format: "yyyy-mm-dd",
            autoclose: true,
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted:  "<?= $ChapterInfo->event_day; ?>"

        });
    });

    $('#addHoilday').on('shown.bs.modal', function (e) {
        $('#chapter_date_holiday').datepicker({
            //format: "yyyy-mm-dd",
            autoclose: true,
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted:  "<?= $ChapterInfo->event_day; ?>"

        });
    });

    $(".edit_holiday").on('click', function () {

        var id = $(this).attr('data_id');
        var name = $(this).attr('data_name');
        var date = $(this).attr('data_date');

        $('#id_update').val('');
        $('#id_update').val(id);
        $('#name_update').val('');
        $('#name_update').val(name);
        $('#chapter_date_holiday_update').val('');
        $('#chapter_date_holiday_update').val(date);
        $('#updateHoilday').modal('show');
    });

    $(".edit_week").on('click', function () {

        var id = $(this).attr('data_id');
        var name = $(this).attr('data_name');
        var date = $(this).attr('data_date');

        $('#id_update_week').val('');
        $('#id_update_week').val(id);
        $('#name_update_week').val('');
        $('#name_update_week').val(name);
        $('#chapter_date_week_update').val('');
        $('#chapter_date_week_update').val(date);
        $('#updateFreeweek').modal('show');
    });

    $('#updateHoilday').on('shown.bs.modal', function (e) {

        $('#chapter_date_holiday_update').datepicker({
            //format: "yyyy-mm-dd",
            autoclose: true,
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted: "<?= $ChapterInfo->event_day; ?>"

        });
    });
    $('#updateFreeweek').on('shown.bs.modal', function (e) {

        $('#chapter_date_week_update').datepicker({
            //format: "yyyy-mm-dd",
            autoclose: true,
            startDate: "<?= getCurrentDate(); ?>",
            daysOfWeekDisabled: <?= $restricted_days ?>,
            datesDisabled: <?= $holidays_days; ?>,
            daysOfWeekHighlighted: "<?= $ChapterInfo->event_day; ?>"

        });
    });
    CKEDITOR.replace('myeditor',{
        /*toolbar :
         [
         /!*  ['Source'],*!/
         /!*['Bold','Italic','Underline','Strike'],*!/
         ],
         height: 200,
         //width: 400*/
    });
</script>
