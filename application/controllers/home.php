<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class Home
 */
class Home extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('chapters_model');
        $this->load->library('email');
        // load form and url helpers
        $this->load->helper(array('form', 'url'));

        // load form_validation library
        $this->load->library('form_validation');
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'BNI ENERGIZER';
        $this->load->model('chapters_model');

        if ($this->isLoggedinFront()) {
            redirect('home/invite_members');
        }

        $data['chapters'] = $this->chapters_model->getAllChapters();

        $this->load->view('front/login', $data);
    }

    /**
     *
     */
    public function login()
    {
        if ($this->isLoggedinFront()) {
            redirect('home/invite_members');
        }

        if ($this->input->post()) {

            $this->form_validation->set_rules('UserId', 'User Name', 'required');
            $this->form_validation->set_rules('phone', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('home');
            } else {

                $result = $this->home_model->loginByEmailPhone($this->input->post('UserId'), $this->input->post('phone'),
                    $this->input->post('chapter'));
                if ($result) {
                    redirect('home/invite_members');
                } else {
                    $this->session->set_flashdata('error', "Login failed. Please use correct username or password!");
                    redirect('home');
                }
            }

        } else {
            redirect('home');
        }
    }

    /**
     *
     */
    public function invite_members()
    {
        if (!$this->isLoggedinFront()) {
            redirect('home');
        }
 
/*
if (mail('se.faisalijaz@gmail.com',"test", "test", $headers = [])) {
        ?>
        <h3 style="color:#d96922; font-weight:bold; height:0px; margin-top:1px;">Thank You For Contacting us!!</h3>

        <?php
    } else {

        print_r(error_get_last());
    }
*/



        $holidays_days = array();
        $holidays_data = $this->home_model->getHolidaysList($this->getIdLoggedinChapterId());
        if (!empty($holidays_data)) {
            foreach ($holidays_data as $holidays_date) {
                $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
            }

        }
        $chapter_info = $this->chapters_model->getOneChapter($this->getIdLoggedinChapterId());

        $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
        $restricted_days = rtrim($restricted_days, ",");
        $data['restricted_days'] = json_encode($restricted_days);
        $data['holidays'] = $holidays_data;
        $data['holidays_days'] = json_encode($holidays_days);

        // $chapter_info = $this->chapters_model->getOneChapter($chapter_id);
        $data['chapter_info'] = $chapter_info;
        $data['chapter_id'] = $this->getIdLoggedinChapterId();


        $data['categories'] = $this->home_model->getAllCategories();
        $data['chapters'] = $this->chapters_model->getAllChapters();
        $data['dates'] = $this->home_model->getDatesByAmdin($this->getIdLoggedinFront(), $this->getIdLoggedinChapterId());
        $data['validation_errors'] = false;

        if ($this->input->post()) {


            // basic required field
            $this->form_validation->set_rules('type', 'Type', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('company', 'Company', 'required');
            $this->form_validation->set_rules('category', 'Category', 'required');
            $this->form_validation->set_rules('date', 'Date', 'required');

            if ($this->input->post('new_category')) {
                $this->form_validation->set_rules('new_category', 'New Category', 'required');
            }

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = true;
                $this->load->view('front/invite_form', $data);
            } else {

                $invitationData = $this->input->post();

                if ($this->input->post('new_category')) {
                    $category = $this->home_model->addNewCategory($invitationData['new_category']);

                    if ($category > 0) {
                        $invitationData['category'] = $category;
                        unset($invitationData['new_category']);
                    }
                }

                $data['email'] = $invitationData;
                $data['CategoryName'] = $this->home_model->getCatName($invitationData['category'])->cat_name;
                $data['chapters'] = $this->chapters_model->getAllChapters();
                $data['adminInfo'] = $this->home_model->adminInfo($this->getIdLoggedinFront());
                $data['chapterInfo'] = $this->chapters_model->getOneChapter($this->getIdLoggedinChapterId());

                //   $data_base[0]['body'] = 'I am a {$club} fan.'; // Tests


                //  echo strtr($data_base[0]['body'], $vars);

                $email_data = $data['chapterInfo']->email_template;
                $vars = array(
                    '{visitor_name}'       => $data['email']['name'],
                    '{chapter_location}'       => $data['chapterInfo']->chap_location,
                    '{chapter_location_map}'       => $data['chapterInfo']->chap_location_map,
                    '{chapter_name}'       => $data['chapterInfo']->chap_name,
                    '{date}'       => date('l, F jS, Y', strtotime($data['email']['date'])),
                    '{chapter_meeting_fee}'       => $data['chapterInfo']->chap_meeting_fee,
                    '{visitor_category}'        => $data['CategoryName'],
                    '{visitor_company}' => $data['email']['company'],
                    '{visitor_phone}' => $data['email']['phone'],
                    '{InvitedBy_name}' => $data['adminInfo']->name,
                    '{InvitedBy_company}' => $data['adminInfo']->usercompany,
                    '{InvitedBy_email}' => $data['adminInfo']->email,
                    '{InvitedBy_mobile}' => $data['adminInfo']->mobile
                );

                $email_data = strtr($email_data, $vars);

             //   $message = $this->load->view('email/BNI_Invitation', $data, TRUE);
                $message = $email_data;

                // mail('se.faisalijaz@gmail.com' , 'Test', 'Test Email');

             //   $Email = $this->SendEmail($this->input->post('email'), 'cs.akashahmed@gmail.com', 'BNI Energizers Invitation', $email_data, $headers = '');

                $Email = $this->SendEmail($this->input->post('email'), $data['adminInfo']->email, 'BNI Energizers Invitation', $message, $headers = '');

                $this->SendEmail($data['adminInfo']->email,'info@visitbni.xyz', 'Copy of BNI Energizers Invitation sent to ('. $this->input->post("email") .')', $message, $headers = '');
 

                $this->home_model->saveInvitation($invitationData, $this->getIdLoggedinFront());
                // $this->load->view('email/BNI_Invitation',$data);
               // $Email = 1;
                if ($Email) {
                    $this->session->set_flashdata('success_message', 'Invitation Sent!');
                } else {
                    $this->session->set_flashdata('error_message', 'Error! Invitation could not be sent.');
                }
 
                redirect(base_url('home/thank_you'));
            }
        }

        $this->load->view('front/invite_form', $data);
    }

    public function invite_members_site()
    {

        /*
        if (mail('se.faisalijaz@gmail.com',"test", "test", $headers = [])) {
                ?>
                <h3 style="color:#d96922; font-weight:bold; height:0px; margin-top:1px;">Thank You For Contacting us!!</h3>

                <?php
            } else {

                print_r(error_get_last());
            }
        */
        //  error_reporting(0);
        $data['validation_errors'] = false;

        if ($this->input->post()) {


            // basic required field
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');



            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = true;
                $this->load->view('front/invite_form_site', $data);
            } else {

                $invitationData = $this->input->post();



                $data['email'] = $invitationData;
              //  $data['CategoryName'] = $this->home_model->getCatName($invitationData['category'])->cat_name;
                $data['chapters'] = $this->chapters_model->getAllChapters();
                $data['adminInfo'] = $this->home_model->adminInfo($this->getIdLoggedinFront());
                $data['chapterInfo'] = $this->chapters_model->getOneChapter($this->getIdLoggedinChapterId());

                $message = $this->load->view('email/BNI_Invitation_site', $data, TRUE);

                // mail('se.faisalijaz@gmail.com' , 'Test', 'Test Email');$Email = $this->SendEmailSite($this->input->post('email'), 'khuram@wistech.biz', 'The Big Business Breakfast', $message, $headers = '');
                 $Email = $this->SendEmail($this->input->post('email'), $data['adminInfo']->email, 'BNI Energizers Invitation', $message, $headers = '');

                // $this->SendEmail($data['adminInfo']->email,'info@visitbni.xyz', 'Copy of BNI Energizers Invitation sent to ('. $this->input->post("email") .')', $message, $headers = '');



                // $this->load->view('email/BNI_Invitation',$data);
                $Email = 1;
                if ($Email) {
                    $this->session->set_flashdata('success_message', 'Invitation Sent!');
                } else {
                    $this->session->set_flashdata('error_message', 'Error! Invitation could not be sent.');
                }

                redirect(base_url('home/thank_you_site'));
            }
        }

        $this->load->view('front/invite_form_site', $data);
    }
    public function thank_you()
    {
        $this->load->view('front/thank_you');
    }
    public function thank_you_site()
    {
        $this->load->view('front/thank_you_site');
    }

    /**
     * @return bool
     */
    public function isLoggedinFront()
    {
        $loggedin = $this->session->userdata('home_logged_in');
        if ($loggedin['isLoggedinFront']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function getIdLoggedinFront()
    {
        $loggedin = $this->session->userdata('home_logged_in');
        if ($loggedin['isLoggedinFront']) {
            return $loggedin['userId'];
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function getIdLoggedinChapterId()
    {
        $loggedin = $this->session->userdata('home_logged_in');
        if ($loggedin['isLoggedinFront']) {
            return $loggedin['ChapterId'];
        } else {
            return false;
        }
    }


    public function findCatByName($name)
    {
        $this->db->select('*');
        $this->db->from('tbl_categories');
        $this->db->where('cat_name', $name);
        return $this->db->get()->row();
    }


    public function logout()
    {
        $this->session->sess_destroy();
        redirect('home/login');
    }


    public function SendEmail($to, $from, $subject, $message, $headers = '')
    {
        $config = [];

        /*
        | -------------------------------------------------------------------
        | EMAIL CONFING
        | -------------------------------------------------------------------
        | Configuration of the outgoing mail server.
        | */
 
        /*
		$config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.mailgun.org';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'postmaster@wisdom.wiscrm.com';
        $config['smtp_pass'] = 'c5003b553c57bf6b977434f2ace168ee';   
        */
		$config['charset'] = 'utf-8';
        $config['set_newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE;

        try {
			
			// $this->email->set_newline("\r\n");
            $this->email->initialize($config);
            $this->load->library('email', $config);
            $this->email->from($from);
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            if (!$this->email->send()) {
                echo "<pre>" . $this->email->print_debugger() . "</pre>";
                print_r($message);
                
            } else {
                return TRUE;
            }

        } catch (Exception  $exception) {
            print_r($exception->getMessage());
            die;
        }
    }
    public function SendEmailSite($to, $from, $subject, $message, $headers = '')
    {
        $config = [];

        /*
        | -------------------------------------------------------------------
        | EMAIL CONFING
        | -------------------------------------------------------------------
        | Configuration of the outgoing mail server.
        | */

        /*
		$config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.mailgun.org';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'postmaster@wisdom.wiscrm.com';
        $config['smtp_pass'] = 'c5003b553c57bf6b977434f2ace168ee';
        */
        $config['charset'] = 'utf-8';
        $config['set_newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE;

        try {

            // $this->email->set_newline("\r\n");
            $this->email->initialize($config);
            $this->load->library('email', $config);
            $this->email->from($from, 'BNI Energizers');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($message);
            if (!$this->email->send()) {
                echo "<pre>" . $this->email->print_debugger() . "</pre>";
                print_r($message);

            } else {
                return TRUE;
            }

        } catch (Exception  $exception) {
            print_r($exception->getMessage());
            die;
        }
    }

    /**
     *
     */
    public function changeStatus()
    {
        if ($this->input->post()) {
            if ($this->home_model->changeStatus($this->input->post('status'), $this->input->post('id'))) {
                echo 1;
                return true;
            }
            echo 0;
            return false;
        }
    }


    public function changeType()
    {
        if ($this->input->post()) {
            if ($this->home_model->changeStatusType($this->input->post('type'), $this->input->post('id'))) {
                echo 1;
                return true;
            }
            echo 0;
            return false;
        }
    }

    public function changeCallStatus()
    {
        if ($this->input->post()) {
            if ($this->home_model->changeStatusCall($this->input->post('call'), $this->input->post('id'))) {
                echo 1;
                return true;
            }
            echo 0;
            return false;
        }
    }

    public function changeVisitStatus()
    {
        if ($this->input->post()) {
            if ($this->home_model->changeVisitedStatus($this->input->post('status'), $this->input->post('id'))) {
                echo 1;
                return true;
            }
            echo 0;
            return false;
        }
    }


    public function changeStatusFormFilled()
    {
        if ($this->input->post()) {
            if ($this->home_model->changeStatusFormFilled($this->input->post('status'), $this->input->post('id'))) {
                echo 1;
                return true;
            }
            echo 0;
            return false;
        }
    }

    /**
     *
     */
    public function getChapterAdmins()
    {
        if ($this->input->post()) {
            $html = '';

            $admins = $this->home_model->getCahpterAdminsByChapter($this->input->post('chapter'));
            $html .= '<option value="">Select User</option>';

            if ($admins <> null) {
                foreach ($admins as $admin) {
                    // if ($admin->roleId <> 1) {
                    $html .= "<option value=" . $admin->userId . ">" . $admin->name . "</option>";
                    // }
                }
            }
            echo $html;
            die;
        }
    }


    public function getChapterDates()
    {
        if ($this->input->post()) {

            $dates = [];
            $html = '';
            $html .= '<option value="">Select Date</option>';

            $getChapterDates = $this->chapters_model->getChapterDates($this->input->post('chapter'));

            if (count($getChapterDates) > 0) {
                foreach ($getChapterDates as $chapterDates) {
                    $dates[] = date('d-m-Y', strtotime($chapterDates->cahpter_date));
                    $html .= "<option value=" . $chapterDates->cahpter_date . ">" . $chapterDates->cahpter_date . "</option>";
                }
            }

            echo $html;
            die;
        }
    }

}