<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Chapter extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('home_model');
        $this->load->model('chapters_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'BNI - Chapters';

        $this->loadViews("dashboard", $this->global, NULL, NULL);
    }

    /**
     * This function is used to load the user list
     */
    function chaptersListing()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {

            if ($this->isChapterAdmin()) {
                $data['chapters'] = $this->home_model->getChaptersbyAdminAssigned($this->session->userdata('userId'));
            } else {
                $data['chapters'] = $this->chapters_model->getAllChapters();
            }

            $this->global['pageTitle'] = 'CodeInsect : User Listing';

            $this->loadViews("chapters/chapters_list", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to load the user list
     */
    function chaptersAdminsList()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {

            $data['chapterAdmins'] = $this->chapters_model->getAllChapterAdmins();

            $this->global['pageTitle'] = 'CodeInsect : User Listing';

            $this->loadViews("chapters/chapter_admins", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['categories'] = $this->home_model->getAllCategories();

            $this->global['pageTitle'] = 'BNI : Add New Chapter';

            $this->loadViews("chapters/addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addChapterAdmin()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['categories'] = $this->home_model->getAllCategories();

            if ($this->isChapterAdmin()) {
                $data['chapters'] = $this->home_model->getChaptersbyAdminAssigned($this->session->userdata('userId'));
            } else {
                $data['chapters'] = $this->chapters_model->getAllChapters();
            }

            $data['users'] = $this->user_model->getUsersList();

            $this->global['pageTitle'] = 'BNI : Add New Chapter';

            $this->loadViews("chapters/addNewChapterAdmin", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to add new user to the system
     */
    function addNewChapter()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean|max_length[512]');
            $this->form_validation->set_rules('location_map', 'Location Map', 'trim|required|xss_clean|max_length[512]');

            if ($this->form_validation->run() == FALSE) {
                $this->addNew();
            } else {

                $result = $this->chapters_model->saveInfo($this->input->post());

                if ($this->isChapterAdmin()) {
                    $data['chapter'] = $result;
                    $data['user'] = $this->session->userdata('userId');
                    $this->chapters_model->saveChapterAdminsInfo($data);
                }

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Chapter created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Chapter creation failed');
                }

                redirect('chapter/chaptersListing');
            }
        }
    }


    /**
     * This function is used to add new user to the system
     */
    function addNewChapterAdmin()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('chapter', 'Chapter', 'trim|required|max_length[1000]|xss_clean');
            $this->form_validation->set_rules('user', 'User', 'trim|required|xss_clean|max_length[512]');

            if ($this->form_validation->run() == FALSE) {
                $this->addNew();
            } else {

                $result = $this->chapters_model->saveChapterAdminsInfo($this->input->post());

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Chapter created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Chapter creation failed');
                }

                redirect('chapter/chaptersAdminsList');
            }
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editChapter($chapter = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($chapter == null) {
                redirect('chapter/chaptersListing');
            }
            $this->load->model('home_model');

            //$days_days = array();
            $freeweeks_days = array();
            $freeweeks_data = $this->home_model->getFreeweekList($chapter);

            if (!empty($freeweeks_data)) {
                foreach ($freeweeks_data as $freeweeks_date) {
                    $freeweeks_days[] = dbToSysDate($freeweeks_date->week_date);
                }

            }
            $data['freeweeks'] = $freeweeks_data;
            $data['freeweeks_days'] = json_encode($freeweeks_days);
            $holidays_days = array();
            $holidays_data = $this->home_model->getHolidaysList($chapter);
            if (!empty($holidays_data)) {
                foreach ($holidays_data as $holidays_date) {
                    $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
                }

            }
            $chapter_info = $this->chapters_model->getOneChapter($chapter);
            $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
            $restricted_days = rtrim($restricted_days, ",");
            $data['restricted_days'] = json_encode($restricted_days);
            $data['holidays'] = $holidays_data;

            $all_free_fee_days = array_merge($freeweeks_days, $holidays_days);

            $data['holidays_days'] = json_encode($holidays_days);

            $data['roles'] = $this->user_model->getUserRoles();
            $data['ChapterInfo'] = $chapter_info;
            $data['ChapterDate'] = $this->chapters_model->getChapterDates($chapter);

            $this->global['pageTitle'] = 'Chapters : Edit Chapter';

            $this->loadViews("chapters/editChap", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editChapterAdmin($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('chapter/chaptersListing');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['ChapterAdmin'] = $this->chapters_model->getChapterAdmin($id);

            if ($this->isChapterAdmin()) {
                $data['chapters'] = $this->home_model->getChaptersbyAdminAssigned($this->session->userdata('userId'));
            } else {
                $data['chapters'] = $this->chapters_model->getAllChapters();
            }

            $data['users'] = $this->user_model->getUsersList();

            $this->global['pageTitle'] = 'Chapters : Edit Chapter';

            $this->loadViews("chapters/editChapterAdmin", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateChapterAdmin()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $chapter_admin_id = $this->input->post('chapter_admin_id');

            $this->form_validation->set_rules('chapter', 'Chapter', 'trim|required|max_length[1000]|xss_clean');
            $this->form_validation->set_rules('user', 'User', 'trim|required|xss_clean|max_length[512]');

            if ($this->form_validation->run() == FALSE) {
                $this->editChapterAdmin($chapter_admin_id);
            } else {

                $result = $this->chapters_model->updateChapterAdminsInfo($this->input->post());

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Chapter updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Chapter updation failed');
                }

                redirect('chapter/chaptersAdminsList');
            }
        }
    }

    /**
     * This function is used to edit the user information
     */
    function saveChapter()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $chapId = $this->input->post('id');

            $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean|max_length[512]');

            if ($this->form_validation->run() == FALSE) {
                $this->editChapter($chapId);
            } else {

                $result = $this->chapters_model->updateInfo($this->input->post());

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Chapter updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Chapter updation failed');
                }

                redirect('chapter/chaptersListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteChapter()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $chapter_id = $this->input->post('chapter_id');

            $result = $this->chapters_model->deleteChapter($chapter_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteChapterAdmin()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $id = $this->input->post('id');

            $result = $this->chapters_model->deleteChapterAdmin($id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'CodeInsect : Change Password';

        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }


    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldPassword', 'Old password', 'required|max_length[20]');
        $this->form_validation->set_rules('newPassword', 'New password', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Confirm new password', 'required|matches[newPassword]|max_length[20]');

        if ($this->form_validation->run() == FALSE) {
            $this->loadChangePass();
        } else {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');

            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);

            if (empty($resultPas)) {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            } else {
                $usersData = array('password' => getHashedPassword($newPassword), 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));

                $result = $this->user_model->changePassword($this->vendorId, $usersData);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Password updation successful');
                } else {
                    $this->session->set_flashdata('error', 'Password updation failed');
                }

                redirect('loadChangePass');
            }
        }
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';

        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function holidaysList($chapter_id)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('home_model');

            $data['holidays'] = $this->home_model->getHolidaysList($chapter_id);

            $this->global['pageTitle'] = 'BNI : Holidays';

            $this->loadViews("chapters/holidays", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to add new user to the system
     */
    function addHoliday()
    {
        $this->global['pageTitle'] = 'BNI : Add Holiday';
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $data = array();

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('holiday_date', 'Date', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = true;
                // $this->loadViews("chapters/addHoliday", $this->global, $data, NULL);
            } else {
                $post = $this->input->post();

                $result = $this->home_model->saveHoliday($this->input->post());

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Holiday created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Holiday creation failed');
                }
                // redirect('chapter/holidaysList');
                redirect('chapter/editChapter/' . $post['chapter_id']);
            }

            $this->loadViews("chapters/addHoliday", $this->global, $data, NULL);
        }
    }

    public function deleteHoliday($id)
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            $holiday_data = $this->home_model->getHolidayById($id);
            $result = $this->home_model->deleteHoliday($id);

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Record Deleted!');
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('chapter/editChapter/' . $holiday_data->chapter_id);
        }
    }

    public function editHoilday()
    {


        /*$data['holiday'] = $this->home_model->getHolidayById($id);
        $data['holiday_id'] = $id;*/

        if ($this->input->post()) {
            $post = $this->input->post();

            $result = $this->home_model->updateHoliday($this->input->post());

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Information updated successfully!');
            } else {
                $this->session->set_flashdata('error', 'Error! Information not updated!');
            }


            // redirect('chapter/editHoilday/' . $id);
            redirect('chapter/editChapter/' . $post['chapter_id']);
        } else {
            redirect('chapter');
        }

    }

    public function dashboard()
    {


        /*$data['holiday'] = $this->home_model->getHolidayById($id);
        $data['holiday_id'] = $id;*/

        if ($this->input->post()) {
            $post = $this->input->post();

            $result = $this->home_model->updateHoliday($this->input->post());

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Information updated successfully!');
            } else {
                $this->session->set_flashdata('error', 'Error! Information not updated!');
            }


            // redirect('chapter/editHoilday/' . $id);
            redirect('chapter/editChapter/' . $post['chapter_id']);
        } else {
            redirect('chapter');
        }

    }

    /**
     * This function is used to load the add new form
     */
    function freeweekList($chapter_id)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('home_model');

            $data['holidays'] = $this->home_model->getFreeweekList($chapter_id);

            $this->global['pageTitle'] = 'BNI : Free weeks';

            $this->loadViews("chapters/freeweeks", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to add new user to the system
     */
    function addFreeweek()
    {
        $this->global['pageTitle'] = 'BNI : Add Freeweek';
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $data = array();
            /*   echo "<pre>";
               print_r($_POST);
               die;*/

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('week_date', 'Date', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['validation_errors'] = true;
            } else {
                $post = $this->input->post();

                $result = $this->home_model->saveFreeweek($this->input->post());

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Free Week created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Free Week creation failed');
                }
                // redirect('chapter/holidaysList');
                redirect('chapter/editChapter/' . $post['chapter_id']);
            }

            $this->loadViews("chapter/editChapter", $this->global, $data, NULL);
        }
    }

    public function deleteFreeweek($id)
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            $holiday_data = $this->home_model->getFreeweekById($id);
            $result = $this->home_model->deleteFreeweek($id);

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Record Deleted!');
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('chapter/editChapter/' . $holiday_data->chapter_id);
        }
    }

    public function editFreeweek()
    {


        /*$data['holiday'] = $this->home_model->getHolidayById($id);
        $data['holiday_id'] = $id;*/

        if ($this->input->post()) {
            $post = $this->input->post();

            $result = $this->home_model->updateFreeweek($this->input->post());

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Information updated successfully!');
            } else {
                $this->session->set_flashdata('error', 'Error! Information not updated!');
            }


            // redirect('chapter/editHoilday/' . $id);
            redirect('chapter/editChapter/' . $post['chapter_id']);
        } else {
            redirect('chapter');
        }

    }

    public function invitation_test()
    {

        $data=array();
        if ($this->input->get()) {
            $from_date = date('Y-m-d', strtotime($this->input->get('from_date')));
            $to_date = date('Y-m-d', strtotime($this->input->get('to_date')));
            if (isset($_GET['visited'])) {
                $visited = $this->input->get('visited');
            } else {
                $visited = 0;
            }
        }
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        $data['visited'] = $visited;

        $this->global['pageTitle'] = 'BNI : Add Freeweek';

        $this->loadViews("chapters/invitations_home", $this->global, $data, NULL);
       // $this->load->view('chapters/invitations_home');
    }


    public function posts()
    {
        $columns = array(
            0 =>'inv_id',
            1 =>'inv_name',
            2=> 'inv_email',
            3=> 'inv_phone',
            4=> 'inv_compnay',
            5=> 'cat_name',
            6=> 'name',
            7=> 'inv_type',
            8=> 'inv_called',
            9=> 'inv_status',
            10=> 'inv_id',
        );



        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $from_date = $this->input->post('from_date');
        $to_date = $this->input->post('to_date');
        $visited = $this->input->post('visited');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];

        $totalData = $this->home_model->allposts_count($from_date,$to_date,$visited);

        $totalFiltered = $totalData;

        if(empty($this->input->post('search')['value']))
        {
            $posts = $this->home_model->allposts($from_date,$to_date,$visited,$limit,$start,$order,$dir);

        }
        else {
            $search = $this->input->post('search')['value'];

            $posts =  $this->home_model->posts_search($from_date,$to_date,$visited,$limit,$start,$search,$order,$dir);

            $totalFiltered = $this->home_model->posts_search_count($from_date,$to_date,$visited,$search);
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                if($post->inv_status == '1'){
                    $inv_status = 'Yes';
                }else if($post->inv_status == '2'){
                    $inv_status = 'Not Picking';
                }else if($post->inv_status == '3'){
                    $inv_status = 'Invite Again';
                }else{
                    $inv_status = 'No';
                }
              //  $nestedData['inv_id'] = $post->inv_id;
                $nestedData['inv_name'] = $post->inv_name;
                $nestedData['inv_email'] = $post->inv_email;
                $nestedData['inv_phone'] = $post->inv_phone;
                $nestedData['inv_compnay'] = $post->inv_compnay;
                $nestedData['cat_name'] = $post->cat_name;
                $nestedData['inv_date'] = date('j M Y h:i a',strtotime($post->inv_date));
                $nestedData['name'] = ucfirst($post->name);
                $nestedData['inv_type'] = ucfirst($post->inv_type);
                $nestedData['inv_called'] = ($post->inv_called == 1)? 'Yes':'No';
                $nestedData['inv_status'] = $inv_status;
                $nestedData['action'] = '<a target="_blank"
                                               href="'.base_url('user/invitation_detail/'.$post->inv_id).'"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a target="_blank"
                                               href="'.base_url('user/edit_invitation/'.$post->inv_id).'"
                                               class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href="'.base_url('user/deleteInvitation/'.$post->inv_id).' "
                <a href="'.base_url('user/deleteInvitation/'.$post->inv_id).'"
                                               class="btn btn-danger btn-sm delete_inv"
                                               id="'. $post->inv_id.'">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>';

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


}