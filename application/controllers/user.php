<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Faisal
 * @version : 1.1
 * @since : 20 October 2017
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('home_model');
        $this->load->model('chapters_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {

        $this->global['pageTitle'] = 'CodeInsect : Dashboard';

        redirect('user/view_invitations');
        // $this->loadViews("dashboard", $this->global, NULL, NULL);
    }

    /**
     *
     */
    public function dashboardDetail()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            if ($this->input->post()) {
                $from_date = date('Y-m-d', strtotime($this->input->post('from_date')));
                $to_date = date('Y-m-d', strtotime($this->input->post('to_date')));

                $data['from_date'] = $this->input->post('from_date');
                $data['to_date'] = $this->input->post('to_date');
                $data['dashboard_invitations'] = $this->user_model->getAllInvitationsByDateDashboard($from_date, $to_date);


            } else {
                $first_day_this_month = date('Y-m-01');
                $last_day_this_month = date('Y-m-t');
                $data['from_date'] = date('m/01/Y');
                $data['to_date'] = date('m/t/Y');
                $data['dashboard_invitations'] = $this->user_model->getAllInvitationsByDateDashboard($first_day_this_month, $last_day_this_month);
            }

            $this->loadViews("chapters/dashboard", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }

    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['userRecords'] = [];
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->user_model->userListingCount($searchText);

            $returns = $this->paginationCompress("userListing/", $count, 25);
            if ($this->isChapterAdmin()) {
                $chap = $this->home_model->getCahpterIdByChapterAdmin($this->getAdminLoggedinId());
                if ($chap <> null) {

                    $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
                }
            } else {
                $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
            }

            $this->global['pageTitle'] = 'CodeInsect : User Listing';

            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['categories'] = $this->home_model->getAllCategories();

            $this->global['pageTitle'] = 'CodeInsect : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if (empty($userId)) {
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }

    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[20]');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role', 'Role', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->addNew();
            } else {
                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
                $company = $this->input->post('company');
                $category = $this->input->post('category');
                $designation = $this->input->post('designation');

                $userInfo = [
                    'email' => $email,
                    'password' => getHashedPassword($password),
                    'roleId' => $roleId,
                    'name' => $name,
                    'mobile' => $mobile,
                    'createdBy' => $this->vendorId,
                    'createdDtm' => date('Y-m-d H:i:s'),
                    'usercategory' => $category,
                    'usercompany' => $company,
                    'designation' => $designation,
                    'chap_id' => $this->session->userdata('chapter_id')
                ];

                if ($this->input->post('new_category') != null && $this->input->post('new_category') != "") {
                    $category = $this->home_model->addNewCategory($this->input->post('new_category'));
                    if ($category > 0) {
                        $userInfo['usercategory'] = $category;
                    }
                }

                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);

                if ($result > 0) {

                    $this->session->set_flashdata('success', 'New User created successfully');

                    if ($this->isChapterAdmin()) {

                        $chapter_id = $this->home_model->getCahpterIdByChapterAdmin($this->getAdminLoggedinId());

                        $data['chapter'] = $chapter_id->ch_chap_id;
                        $data['user'] = $result;
                        $this->chapters_model->saveChapterAdminsInfo($data);
                    }

                } else {
                    $this->session->set_flashdata('error', 'User creation failed');
                }

                redirect('addNew');
            }
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if ($this->isAdmin() == TRUE || $userId == 1) {
            $this->loadThis();
        } else {
            if ($userId == null) {
                redirect('userListing');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            $data['categories'] = $this->home_model->getAllCategories();

            $this->global['pageTitle'] = 'CodeInsect : Edit User';

            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $userId = $this->input->post('userId');

            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password', 'Password', 'matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'matches[password]|max_length[20]');
            $this->form_validation->set_rules('role', 'Role', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->editOld($userId);
            } else {

                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
                $company = $this->input->post('company');
                $category = $this->input->post('category');
                $designation = $this->input->post('designation');

                $userInfo = array();

                if (empty($password)) {
                    $userInfo = array('email' => $email, 'roleId' => $roleId, 'name' => $name,
                        'mobile' => $mobile, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'), 'designation' => $designation,
                        'usercategory' => $category, 'usercompany' => $company);
                } else {
                    $userInfo = array('email' => $email, 'password' => getHashedPassword($password), 'roleId' => $roleId,
                        'name' => ucwords($name), 'mobile' => $mobile, 'updatedBy' => $this->vendorId, 'designation' => $designation,
                        'updatedDtm' => date('Y-m-d H:i:s'), 'usercategory' => $category, 'usercompany' => $company);
                }

                if ($this->input->post('new_category') != null && $this->input->post('new_category') != "") {
                    $category = $this->home_model->addNewCategory($this->input->post('new_category'));
                    if ($category > 0) {
                        $userInfo['usercategory'] = $category;
                    }
                }

                $result = $this->user_model->editUser($userInfo, $userId);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'User updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'User updation failed');
                }

                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));

            $result = $this->user_model->deleteUser($userId, $userInfo);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'CodeInsect : Change Password';

        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }


    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldPassword', 'Old password', 'required|max_length[20]');
        $this->form_validation->set_rules('newPassword', 'New password', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Confirm new password', 'required|matches[newPassword]|max_length[20]');

        if ($this->form_validation->run() == FALSE) {
            $this->loadChangePass();
        } else {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');

            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);

            if (empty($resultPas)) {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            } else {
                $usersData = array('password' => getHashedPassword($newPassword), 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));

                $result = $this->user_model->changePassword($this->vendorId, $usersData);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Password updation successful');
                } else {
                    $this->session->set_flashdata('error', 'Password updation failed');
                }

                redirect('loadChangePass');
            }
        }
    }

    /**
     *
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';

        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     *
     */
    public function view_invitations()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            if ($this->input->get()) {
                $from_date = date('Y-m-d', strtotime($this->input->get('from_date')));
                $to_date = date('Y-m-d', strtotime($this->input->get('to_date')));
                if (isset($_GET['visited'])) {
                    $visited = $this->input->get('visited');
                } else {
                    $visited = 0;
                }
                $data['invitations'] = $this->home_model->getAllInvitationsByDate($from_date, $to_date, $visited);
            } else {

                $date = new DateTime();
                $date->modify('next tuesday');
                $next_meeting = $date->format('Y-m-d');

                $data['invitations'] = $this->home_model->getAllInvitationsByDate(date('Y-m-d'), $next_meeting, 0);

                // $data['invitations'] = $this->home_model->getAllInvitations();
            }

            $this->loadViews("chapters/invitations", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }

    /**
     *
     */


    /**
     * @param $id
     */
    public function invitation_detail($id)
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['invt'] = $this->home_model->getAllInvitationsById($id);
            $data['users'] = $this->home_model->getAdmins();
            $this->loadViews("chapters/invitation_details", $this->global, $data, NULL);

        } else {
            redirect('/login');
        }

    }

    /**
     *
     */
    public function update_invitation()
    {
        if ($this->input->post()) {

            $result = $this->home_model->updateInvitationDetail($this->input->post());

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Information updated successfully!');
            } else {
                $this->session->set_flashdata('error', 'Error! Information not updated!');
            }

            redirect('user/invitation_detail/' . $this->input->post('invitationId'));
        }
    }

    /**
     *
     */
    public function add_invitation()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {

                $result = $this->home_model->adminInvitation($this->input->post());

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }

                redirect('user/add_invitation');
            }

            $this->loadViews("chapters/add_invitation_details", $this->global, $data, NULL);

        } else {
            redirect('/login');
        }

    }


    public function edit_invitation($id)
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['invt'] = $this->home_model->getAllInvitationsById($id);
            $data['invt_id'] = $id;
            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {

                $result = $this->home_model->adminInvitation($this->input->post(), $id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }

                redirect('user/edit_invitation/' . $id);
            }

            $this->loadViews("chapters/edit_invitation", $this->global, $data, NULL);

        } else {
            redirect('/login');
        }

    }

    public function deleteInvitation($id)
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            $result = $this->home_model->deleteInvitation($id);

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Record Deleted!');
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('user/view_invitations');
        }
    }

    public function getAdminLoggedinId()
    {
        if ($this->session->userdata('isLoggedIn')) {
            return $this->session->userdata('userId');
        }
        return false;
    }

    /**
     *weekly closure
     */
    public function weekly_closure()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            $chapter_id = (int)$this->session->userdata('chapter_id');

            if ($this->input->get()) {
                $next_meeting = sysToDbDate($this->input->get('from_date'));
                $data['chapter_id'] = $chapter_id;
                $data['next_meeting'] = $this->input->get('from_date');
                $data['invitations'] = $this->home_model->getAllInvitationsByDateWeekly($next_meeting, $chapter_id);


            } else {


                $chapter_detail = $this->chapters_model->getOneChapter($chapter_id);
                $event_day = $chapter_detail->event_day;
                $event_day_name = '';
                if ($event_day == 0) {
                    $event_day_name = 'sunday';
                } else if ($event_day == 1) {
                    $event_day_name = 'monday';
                } else if ($event_day == 2) {
                    $event_day_name = 'tuesday';
                } else if ($event_day == 3) {
                    $event_day_name = 'wednesday';
                } else if ($event_day == 4) {
                    $event_day_name = 'thursday';
                } else if ($event_day == 5) {
                    $event_day_name = 'friday';
                } else if ($event_day == 6) {
                    $event_day_name = 'saturday';
                }


                $date = new DateTime();

                $date->modify('next ' . $event_day_name);
                $next_meeting = $date->format('Y-m-d');

                $data['chapter_id'] = $chapter_id;
                $data['next_meeting'] = dbToSysDate($next_meeting);

                $data['invitations'] = $this->home_model->getAllInvitationsByDateWeekly($next_meeting, $chapter_id);
            }
            // $data['invitations'] = $this->home_model->getAllInvitations();


            $holidays_days = array();
            $holidays_data = $this->home_model->getHolidaysList($chapter_id);
            if (!empty($holidays_data)) {
                foreach ($holidays_data as $holidays_date) {
                    $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
                }

            }
            $chapter_info = $this->chapters_model->getOneChapter($chapter_id);

            $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
            $restricted_days = rtrim($restricted_days, ",");
            $data['restricted_days'] = json_encode($restricted_days);
            $data['holidays'] = $holidays_data;
            $data['holidays_days'] = json_encode($holidays_days);

            // $chapter_info = $this->chapters_model->getOneChapter($chapter_id);
            $data['chapter_info'] = $chapter_info;
            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            $this->loadViews("chapters/weekly_closure", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }

    public function add_invitation_weekly()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();

                $post['inv_voting'] = "0";
                $post['inv_selection'] = "0";
                $post['inv_type'] = 'visitor';
                $post['inv_confirmation'] = "0";
                $post['inv_isCalled'] = "0";
                $post['visited'] = "0";

                $result = $this->home_model->adminInvitation_weekly($post);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['current_url'] != '') {
                    redirect('user/weekly_closure?from_date=' . $post['current_url']);
                } else {
                    redirect('user/weekly_closure');
                }
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_closure');
        }

    }

    public function update_invitation_weekly()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $inv_id = $post['inv_id'];


                $result = $this->home_model->adminInvitation_weeklyUpdate($post, $inv_id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['current_url'] != '') {
                    redirect('user/weekly_closure?from_date=' . $post['current_url']);
                } else {
                    redirect('user/weekly_closure');
                }
                //  redirect('user/weekly_closure');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_closure');
        }

    }

    /**
     *app status
     */
    public function app_status()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {

            $chapter_id = $this->session->userdata('chapter_id');

            if ($this->input->get()) {
                $selection_status = $this->input->get('selection_status');
                $data['chapter_id'] = $chapter_id;
                $data['selection_status'] = $selection_status;

                if (isset($_GET['from_date']) && isset($_GET['to_date'])) {
                    $data['invitations'] = $this->home_model->getAppStatus($chapter_id, $selection_status, $_GET['from_date'], $_GET['to_date']);
                } else {
                    $data['invitations'] = $this->home_model->getAppStatus($chapter_id, $selection_status);
                }
            } else {
                // selection_status

                $data['chapter_id'] = $chapter_id;
                $selection_status = "all";
                $data['selection_status'] = $selection_status;
                $data['invitations'] = $this->home_model->getAppStatus($chapter_id, $selection_status);
            }
            $data['chapters'] = $this->chapters_model->getAllChapters();
            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            $this->loadViews("chapters/app_status", $this->global, $data, NULL);/**/


        } else {
            redirect('/login');
        }
    }

    public function add_app_status()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $post['inv_voting'] = 0;
                $post['inv_selection'] = "0";
                $post['inv_type'] = 'visitor';
                $post['inv_confirmation'] = 0;
                $post['inv_isCalled'] = 0;
                $post['visited'] = 0;

                $result = $this->home_model->adminInvitation_weekly($post);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }

                redirect('user/app_status');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/app_status');
        }

    }

    public function update_app_status()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['categories'] = $this->home_model->getAllCategories();
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $inv_id = $post['inv_id'];


                $result = $this->home_model->appStatusUpdate($post, $inv_id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }

                if ($post['current_url'] != '') {
                    redirect('user/app_status?selection_status=' . $post['current_url']);
                } else {
                    redirect('user/app_status');
                }

                //redirect('user/app_status');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/app_status');
        }

    }


    /**
     *weekly fee member
     */
    public function weekly_fee()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            $chapter_id = (int)$this->session->userdata('chapter_id');

            if ($this->input->get()) {
                $next_meeting = sysToDbDate($this->input->get('from_date'));
                $data['chapter_id'] = $chapter_id;
                $from_date = date('Y-m-d', strtotime($this->input->get('from_date')));

                $data['from_date'] = $this->input->get('from_date');
                $data['next_meeting'] = $this->input->get('from_date');
                if (isset($_GET['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($this->input->get('to_date')));;
                    $data['to_date'] = $this->input->get('to_date');
                } else {
                    // $data['to_date'] = 0;
                    $to_date = 0;
                }
                $data['weeklyfees'] = $this->home_model->getAllWeeklyFee($chapter_id, $from_date, $to_date);

            } else {

                $chapter_detail = $this->chapters_model->getOneChapter($chapter_id);
                $event_day = $chapter_detail->event_day;
                $event_day_name = '';
                if ($event_day == 0) {
                    $event_day_name = 'sunday';
                } else if ($event_day == 1) {
                    $event_day_name = 'monday';
                } else if ($event_day == 2) {
                    $event_day_name = 'tuesday';
                } else if ($event_day == 3) {
                    $event_day_name = 'wednesday';
                } else if ($event_day == 4) {
                    $event_day_name = 'thursday';
                } else if ($event_day == 5) {
                    $event_day_name = 'friday';
                } else if ($event_day == 6) {
                    $event_day_name = 'saturday';
                }

                $date = new DateTime();
                $date->modify('next ' . $event_day_name);
                $next_meeting = $date->format('Y-m-d');
                $data['chapter_id'] = $chapter_id;
                $data['next_meeting'] = dbToSysDate($next_meeting);
                $data['from_date'] = date('m/d/Y');
                $data['to_date'] = dbToSysDate($next_meeting);;

                $data['weeklyfees'] = $this->home_model->getAllWeeklyFee($chapter_id, $next_meeting, 0);
            }
            // $data['invitations'] = $this->home_model->getAllInvitations();


            //$days_days = array();
            $freeweeks_days = array();
            $freeweeks_data = $this->home_model->getFreeweekList($chapter_id);

            if (!empty($freeweeks_data)) {
                foreach ($freeweeks_data as $freeweeks_date) {
                    $freeweeks_days[] = dbToSysDate($freeweeks_date->week_date);
                }

            }
            $data['freeweeks'] = $freeweeks_data;
            $data['freeweeks_days'] = json_encode($freeweeks_days);


            $holidays_days = array();
            $holidays_data = $this->home_model->getHolidaysList($chapter_id);
            if (!empty($holidays_data)) {
                foreach ($holidays_data as $holidays_date) {
                    $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
                }

            }
            $chapter_info = $this->chapters_model->getOneChapter($chapter_id);

            $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
            $restricted_days = rtrim($restricted_days, ",");
            $data['restricted_days'] = json_encode($restricted_days);
            $data['holidays'] = $holidays_data;
            $all_free_fee_days = array_merge($freeweeks_days, $holidays_days);
            $data['holidays_days'] = json_encode($all_free_fee_days);

            // $chapter_info = $this->chapters_model->getOneChapter($chapter_id);
            $data['chapter_info'] = $chapter_info;
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            $this->loadViews("chapters/weekly_fee", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }
    public function add_weekly_fee()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $post['from_date'] = date('m/d/Y');
                $post['to_date'] = $post['week_date'];
                $result = $this->home_model->add_weekly_fee($post);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_fee?from_date=' . $post['to_date']);
                } else {
                    redirect('user/weekly_fee');
                }
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_fee');
        }

    }

    public function update_weekly_fee()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $inv_id = $post['id'];


                $result = $this->home_model->update_weekly_fee($post, $inv_id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_fee?from_date=' . $post['from_date']);
                } else {
                    redirect('user/weekly_fee');
                }
                //  redirect('user/weekly_closure');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_closure');
        }

    }

    public function delete_weekly_fee()
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            if (isset($_GET['id']) && $_GET['del_date']) {
                $id = $_GET['id'];
                $result = $this->home_model->deleteWeekFee($id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Record Deleted!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Record not deleted!');
                }
                redirect('user/weekly_fee?from_date=' . $_GET['del_date']);
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('user/weekly_fee');
        }
    }


    /**
     *weekly Receipts member
     */
    public function weekly_receipts()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            $chapter_id = (int)$this->session->userdata('chapter_id');

            if ($this->input->get()) {
                $next_meeting = sysToDbDate($this->input->get('from_date'));
                $data['chapter_id'] = $chapter_id;
                $from_date = date('Y-m-d', strtotime($this->input->get('from_date')));

                $data['from_date'] = $this->input->get('from_date');
                $data['next_meeting'] = $this->input->get('from_date');
                if (isset($_GET['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($this->input->get('to_date')));;
                    $data['to_date'] = $this->input->get('to_date');
                } else {
                    // $data['to_date'] = 0;
                    $to_date = 0;
                }
                $data['weeklyfees'] = $this->home_model->getAllWeeklyReceipts($chapter_id, $from_date, $to_date);

            } else {

                $chapter_detail = $this->chapters_model->getOneChapter($chapter_id);
                $event_day = $chapter_detail->event_day;
                $event_day_name = '';
                if ($event_day == 0) {
                    $event_day_name = 'sunday';
                } else if ($event_day == 1) {
                    $event_day_name = 'monday';
                } else if ($event_day == 2) {
                    $event_day_name = 'tuesday';
                } else if ($event_day == 3) {
                    $event_day_name = 'wednesday';
                } else if ($event_day == 4) {
                    $event_day_name = 'thursday';
                } else if ($event_day == 5) {
                    $event_day_name = 'friday';
                } else if ($event_day == 6) {
                    $event_day_name = 'saturday';
                }

                $date = new DateTime();
                $date->modify('next ' . $event_day_name);
                $next_meeting = $date->format('Y-m-d');
                $data['chapter_id'] = $chapter_id;
                $data['next_meeting'] = dbToSysDate($next_meeting);
                $data['from_date'] = date('m/d/Y');
                $data['to_date'] = dbToSysDate($next_meeting);;

                $data['weeklyfees'] = $this->home_model->getAllWeeklyReceipts($chapter_id, $next_meeting, 0);
            }
            // $data['invitations'] = $this->home_model->getAllInvitations();


            //$days_days = array();
            $freeweeks_days = array();
            $freeweeks_data = $this->home_model->getFreeweekList($chapter_id);

            if (!empty($freeweeks_data)) {
                foreach ($freeweeks_data as $freeweeks_date) {
                    $freeweeks_days[] = dbToSysDate($freeweeks_date->week_date);
                }

            }
            $data['freeweeks'] = $freeweeks_data;
            $data['freeweeks_days'] = json_encode($freeweeks_days);


            $holidays_days = array();
            $holidays_data = $this->home_model->getHolidaysList($chapter_id);
            if (!empty($holidays_data)) {
                foreach ($holidays_data as $holidays_date) {
                    $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
                }

            }
            $chapter_info = $this->chapters_model->getOneChapter($chapter_id);

            $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
            $restricted_days = rtrim($restricted_days, ",");
            $data['restricted_days'] = json_encode($restricted_days);
            $data['holidays'] = $holidays_data;
            $all_free_fee_days = array_merge($freeweeks_days, $holidays_days);
            $data['holidays_days'] = json_encode($all_free_fee_days);

            // $chapter_info = $this->chapters_model->getOneChapter($chapter_id);
            $data['chapter_info'] = $chapter_info;
            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            $this->loadViews("chapters/weekly_receipts", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }

    public function add_weekly_receipts()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $post['from_date'] = date('m/d/Y');
                $post['to_date'] = $post['week_date'];
                $result = $this->home_model->add_weekly_receipt($post);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_receipts?from_date=' . $post['to_date']);
                } else {
                    redirect('user/weekly_receipts');
                }
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_receipts');
        }

    }

    public function update_weekly_receipts()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $inv_id = $post['id'];


                $result = $this->home_model->update_weekly_receipt($post, $inv_id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_receipts?from_date=' . $post['from_date']);
                } else {
                    redirect('user/weekly_receipts');
                }
                //  redirect('user/weekly_closure');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_receipts');
        }

    }

    public function delete_weekly_receipts()
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            if (isset($_GET['id']) && $_GET['del_date']) {
                $id = $_GET['id'];
                $result = $this->home_model->deleteWeekReceipt($id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Record Deleted!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Record not deleted!');
                }
                redirect('user/weekly_receipts?from_date=' . $_GET['del_date']);
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('user/weekly_receipts');
        }
    }

    /**
     *weekly fee visitor
     */
    public function weekly_visitor_fee()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            $chapter_id = (int)$this->session->userdata('chapter_id');

            if ($this->input->get()) {
                $next_meeting = sysToDbDate($this->input->get('from_date'));
                $data['chapter_id'] = $chapter_id;
                $from_date = date('Y-m-d', strtotime($this->input->get('from_date')));

                $data['from_date'] = $this->input->get('from_date');
                $data['next_meeting'] = $this->input->get('from_date');
                if (isset($_GET['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($this->input->get('to_date')));;
                    $data['to_date'] = $this->input->get('to_date');
                } else {
                    // $data['to_date'] = 0;
                    $to_date = 0;
                }
                $data['users'] = $this->home_model->getAllInvitationsVistors($from_date);
                $data['weeklyfees'] = $this->home_model->getAllWeeklyVisitorFee($chapter_id, $from_date, $to_date);

            } else {

                $chapter_detail = $this->chapters_model->getOneChapter($chapter_id);
                $event_day = $chapter_detail->event_day;
                $event_day_name = '';
                if ($event_day == 0) {
                    $event_day_name = 'sunday';
                } else if ($event_day == 1) {
                    $event_day_name = 'monday';
                } else if ($event_day == 2) {
                    $event_day_name = 'tuesday';
                } else if ($event_day == 3) {
                    $event_day_name = 'wednesday';
                } else if ($event_day == 4) {
                    $event_day_name = 'thursday';
                } else if ($event_day == 5) {
                    $event_day_name = 'friday';
                } else if ($event_day == 6) {
                    $event_day_name = 'saturday';
                }

                $date = new DateTime();
                $date->modify('next ' . $event_day_name);
                $next_meeting = $date->format('Y-m-d');
                $data['chapter_id'] = $chapter_id;
                $data['next_meeting'] = dbToSysDate($next_meeting);
                $data['from_date'] = date('m/d/Y');
                $data['to_date'] = dbToSysDate($next_meeting);;

                //$data['users'] = $this->home_model->getAllInvitationsVistors("2020-03-10");

                $data['users'] = $this->home_model->getAllInvitationsVistors($next_meeting);
                $data['weeklyfees'] = $this->home_model->getAllWeeklyFee($chapter_id, $next_meeting, 0);
            }
            // $data['invitations'] = $this->home_model->getAllInvitations();


            //$days_days = array();
            $freeweeks_days = array();
            $freeweeks_data = $this->home_model->getFreeweekList($chapter_id);

            if (!empty($freeweeks_data)) {
                foreach ($freeweeks_data as $freeweeks_date) {
                    $freeweeks_days[] = dbToSysDate($freeweeks_date->week_date);
                }

            }
            $data['freeweeks'] = $freeweeks_data;
            $data['freeweeks_days'] = json_encode($freeweeks_days);


            $holidays_days = array();
            $holidays_data = $this->home_model->getHolidaysList($chapter_id);
            if (!empty($holidays_data)) {
                foreach ($holidays_data as $holidays_date) {
                    $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
                }

            }
            $chapter_info = $this->chapters_model->getOneChapter($chapter_id);

            $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
            $restricted_days = rtrim($restricted_days, ",");
            $data['restricted_days'] = json_encode($restricted_days);
            $data['holidays'] = $holidays_data;
            $all_free_fee_days = array_merge($freeweeks_days, $holidays_days);
            $data['holidays_days'] = json_encode($all_free_fee_days);

            // $chapter_info = $this->chapters_model->getOneChapter($chapter_id);
            $data['chapter_info'] = $chapter_info;

            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            $this->loadViews("chapters/weekly_visitor_fee", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }

    public function add_weekly_visitor_fee()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $post['from_date'] = date('m/d/Y');
                $post['to_date'] = $post['week_date'];
                $result = $this->home_model->add_weekly_visitor_fee($post);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_visitor_fee?from_date=' . $post['to_date']);
                } else {
                    redirect('user/weekly_visitor_fee');
                }
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_visitor_fee');
        }

    }

    public function update_weekly_visitor_fee()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $inv_id = $post['id'];


                $result = $this->home_model->update_weekly_visitor_fee($post, $inv_id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_visitor_fee?from_date=' . $post['from_date']);
                } else {
                    redirect('user/weekly_visitor_fee');
                }
                //  redirect('user/weekly_closure');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_visitor_fee');
        }

    }

    public function delete_weekly_visitor_fee()
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            if (isset($_GET['id']) && $_GET['del_date']) {
                $id = $_GET['id'];
                $result = $this->home_model->deleteWeekVisitorFee($id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Record Deleted!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Record not deleted!');
                }
                redirect('user/weekly_visitor_fee?from_date=' . $_GET['del_date']);
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('user/weekly_visitor_fee');
        }
    }

    /**
     *weekly fee visitor
     */
    public function weekly_visitor_receipts()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == false) {
            $chapter_id = (int)$this->session->userdata('chapter_id');

            if ($this->input->get()) {
                $next_meeting = sysToDbDate($this->input->get('from_date'));
                $data['chapter_id'] = $chapter_id;
                $from_date = date('Y-m-d', strtotime($this->input->get('from_date')));

                $data['from_date'] = $this->input->get('from_date');
                $data['next_meeting'] = $this->input->get('from_date');
                if (isset($_GET['to_date'])) {
                    $to_date = date('Y-m-d', strtotime($this->input->get('to_date')));;
                    $data['to_date'] = $this->input->get('to_date');
                } else {
                    // $data['to_date'] = 0;
                    $to_date = 0;
                }
                $data['users'] = $this->home_model->getAllInvitationsVistors($from_date);
                $data['weeklyfees'] = $this->home_model->getAllWeeklyVisitorReceipt($chapter_id, $from_date, $to_date);

            } else {

                $chapter_detail = $this->chapters_model->getOneChapter($chapter_id);
                $event_day = $chapter_detail->event_day;
                $event_day_name = '';
                if ($event_day == 0) {
                    $event_day_name = 'sunday';
                } else if ($event_day == 1) {
                    $event_day_name = 'monday';
                } else if ($event_day == 2) {
                    $event_day_name = 'tuesday';
                } else if ($event_day == 3) {
                    $event_day_name = 'wednesday';
                } else if ($event_day == 4) {
                    $event_day_name = 'thursday';
                } else if ($event_day == 5) {
                    $event_day_name = 'friday';
                } else if ($event_day == 6) {
                    $event_day_name = 'saturday';
                }

                $date = new DateTime();
                $date->modify('next ' . $event_day_name);
                $next_meeting = $date->format('Y-m-d');
                $data['chapter_id'] = $chapter_id;
                $data['next_meeting'] = dbToSysDate($next_meeting);
                $data['from_date'] = date('m/d/Y');
                $data['to_date'] = dbToSysDate($next_meeting);;

                //$data['users'] = $this->home_model->getAllInvitationsVistors("2020-03-10");

                $data['users'] = $this->home_model->getAllInvitationsVistors($next_meeting);
                $data['weeklyfees'] = $this->home_model->getAllWeeklyVisitorReceipt($chapter_id, $next_meeting, 0);
            }
            // $data['invitations'] = $this->home_model->getAllInvitations();


            //$days_days = array();
            $freeweeks_days = array();
            $freeweeks_data = $this->home_model->getFreeweekList($chapter_id);

            if (!empty($freeweeks_data)) {
                foreach ($freeweeks_data as $freeweeks_date) {
                    $freeweeks_days[] = dbToSysDate($freeweeks_date->week_date);
                }

            }
            $data['freeweeks'] = $freeweeks_data;
            $data['freeweeks_days'] = json_encode($freeweeks_days);


            $holidays_days = array();
            $holidays_data = $this->home_model->getHolidaysList($chapter_id);
            if (!empty($holidays_data)) {
                foreach ($holidays_data as $holidays_date) {
                    $holidays_days[] = dbToSysDate($holidays_date->holiday_date);
                }

            }
            $chapter_info = $this->chapters_model->getOneChapter($chapter_id);

            $restricted_days = str_replace($chapter_info->event_day . ",", "", "0,1,2,3,4,5,6,");
            $restricted_days = rtrim($restricted_days, ",");
            $data['restricted_days'] = json_encode($restricted_days);
            $data['holidays'] = $holidays_data;
            $all_free_fee_days = array_merge($freeweeks_days, $holidays_days);
            $data['holidays_days'] = json_encode($all_free_fee_days);

            // $chapter_info = $this->chapters_model->getOneChapter($chapter_id);
            $data['chapter_info'] = $chapter_info;

            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            $this->loadViews("chapters/weekly_visitor_receipt", $this->global, $data, NULL);/**/
        } else {
            redirect('/login');
        }
    }

    public function add_weekly_visitor_receipt()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $post['from_date'] = date('m/d/Y');
                $post['to_date'] = $post['week_date'];
                $result = $this->home_model->add_weekly_visitor_receipt($post);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_visitor_receipts?from_date=' . $post['to_date']);
                } else {
                    redirect('user/weekly_visitor_receipts');
                }
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_visitor_receipts');
        }

    }

    public function update_weekly_visitor_receipt()
    {
        if ($this->isAdmin() == false || $this->isChapterAdmin() == true) {

            $data['users'] = $this->home_model->getAdmins();
            $data['adminLoggedinId'] = $this->getAdminLoggedinId();

            if ($this->input->post()) {
                $post = $this->input->post();
                $inv_id = $post['id'];


                $result = $this->home_model->update_weekly_visitor_receipt($post, $inv_id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Information updated successfully!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Information not updated!');
                }
                if ($post['from_date'] != '') {
                    redirect('user/weekly_visitor_receipts?from_date=' . $post['from_date']);
                } else {
                    redirect('user/weekly_visitor_receipts');
                }
                //  redirect('user/weekly_closure');
            }

        } else {
            $this->session->set_flashdata('error', 'Error! Information not updated!');
            redirect('user/weekly_visitor_receipts');
        }

    }

    public function delete_weekly_visitor_receipt()
    {

        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {

            if (isset($_GET['id']) && $_GET['del_date']) {
                $id = $_GET['id'];
                $result = $this->home_model->deleteWeekVisitorReceipt($id);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Record Deleted!');
                } else {
                    $this->session->set_flashdata('error', 'Error! Record not deleted!');
                }
                redirect('user/weekly_visitor_receipts?from_date=' . $_GET['del_date']);
            } else {
                $this->session->set_flashdata('error', 'Error! Record not deleted!');
            }

            redirect('user/weekly_visitor_receipts');
        }
    }


}