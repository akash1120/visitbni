<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId', 'left');
        $this->db->join('tbl_categories', 'BaseTbl.usercategory = tbl_categories.cat_id', 'left');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId', 'left');
        $this->db->join('tbl_categories', 'BaseTbl.usercategory = tbl_categories.cat_id', 'left');
        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->where('BaseTbl.chap_id', $this->session->userdata('chapter_id'));
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * @param string $searchText
     * @param $page
     * @param $segment
     * @return mixed
     */
    function userListingByChap($searchText = '', $page, $segment, $chap)
    {
        $this->db->select('*');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId', 'left');
        $this->db->join('tbl_categories', 'BaseTbl.usercategory = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_chapters_admins', 'tbl_chapters_admins.ch_admin_id = BaseTbl.userId', 'left');

        if (!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->where('tbl_chapters_admins.ch_chap_id', $chap);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);
        $this->db->where("isDeleted", 0);
        if ($userId != 0) {
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_users', $userInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('roleId !=', 1);
        $this->db->where('userId', $userId);
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);

        return TRUE;
    }


    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);

        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');

        $user = $query->result();

        if (!empty($user)) {
            if (verifyHashedPassword($oldPassword, $user[0]->password)) {
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);

        return $this->db->affected_rows();
    }

    public function getUsersList()
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        return $this->db->get()->result();
    }
    public function getAllInvitationsByDateDashboard($from_date, $to_date)
    {
        $array_results=array();
        $array_results['invited'] = 0;
        $array_results['visited'] = 0;
        $array_results['form_filled'] = 0;
        $array_results['interviewed'] = 0;
        $array_results['selected'] = 0;
        $array_results['rejected'] = 0;


        $invited_query = $this->db->select('*')->from('tbl_invitations')->where("inv_date >=", $from_date)->where("inv_date <=", $to_date)->order_by("inv_id", "DESC")->get();
        if (count($invited_query->result()) > 0) {
            $array_results['invited'] = count($invited_query->result());
        }
        $visited_query = $this->db->select('*')->from('tbl_invitations')->where("inv_date >=", $from_date)->where("inv_date <=", $to_date)->where("inv_visited", '1')->order_by("inv_id", "DESC")->get();
        if (count($visited_query->result()) > 0) {
            $array_results['visited'] = count($visited_query->result());
        }
        $form_filled_query = $this->db->select('*')->from('tbl_invitations')->where("inv_date >=", $from_date)->where("inv_date <=", $to_date)->where("inv_form_filled", '1')->order_by("inv_id", "DESC")->get();
        if (count($form_filled_query->result()) > 0) {
            $array_results['form_filled'] = count($form_filled_query->result());
        }
        $interviewed = $this->db->select('*')->from('tbl_invitations')->where("inv_date >=", $from_date)->where("inv_date <=", $to_date)->where("inv_selection_status", '6')->order_by("inv_id", "DESC")->get();
        if (count($interviewed->result()) > 0) {
            $array_results['interviewed'] = count($interviewed->result());
        }
        $selected = $this->db->select('*')->from('tbl_invitations')->where("inv_date >=", $from_date)->where("inv_date <=", $to_date)->where("inv_selection_status", '3')->order_by("inv_id", "DESC")->get();
        if (count($selected->result()) > 0) {
            $array_results['selected'] = count($selected->result());
        }
        $rejected = $this->db->select('*')->from('tbl_invitations')->where("inv_date >=", $from_date)->where("inv_date <=", $to_date)->where("inv_selection_status", '2')->order_by("inv_id", "DESC")->get();
        if (count($rejected->result()) > 0) {
            $array_results['rejected'] = count($rejected->result());
        }



        return $array_results;
    }
}

  