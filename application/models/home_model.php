<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model
{
    /**
     * @param $id
     * @param $phone
     * @param $chapter
     * @return bool
     */
    function loginByEmailPhone($id, $phone, $chapter)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where(['userId' => $id, 'mobile' => $phone, 'isDeleted' => '0']);
        $result = $this->db->get()->row();

        if (count($result) > 0 && $result <> null) {

            $userData = [
                'isLoggedinFront' => true,
                'UserEmail' => $result->email,
                'userId' => $result->userId,
                'mobile' => $result->mobile,
                'roleId' => $result->roleId,
                'ChapterId' => $chapter
            ];
            $this->session->set_userdata('home_logged_in', $userData);
            return true;
        } else {
            return false;
        }

    }

    /**
     * @return mixed
     */
    public function getAllCategories()
    {
        $this->db->select('*');
        $this->db->from('tbl_categories');
        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCatName($id)
    {
        $this->db->select('cat_name');
        $this->db->from('tbl_categories');
        $this->db->where('cat_id', $id);
        return $this->db->get()->row();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function adminInfo($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('userId', $id);
        return $this->db->get()->row();
    }

    /**
     * @param $data
     * @param $id
     */
    public function saveInvitation($data, $id)
    {

        $table = [
            'inv_type' => $data['type'],
            'inv_name' => $data['name'],
            'inv_email' => $data['email'],
            'inv_phone' => $data['phone'],
            'inv_category' => $data['category'],
            'inv_compnay' => $data['company'],
            'inv_date' => sysToDbDate($data['date']),
            'inv_invitedBy' => $id,
            // 'inv_type' => $data['type'],
        ];

        $this->db->insert('tbl_invitations', $table);
    }

    public function saveInvitation_site($data)
    {

        $table = [

            'inv_name' => $data['name'],
            'inv_email' => $data['email'],
            'inv_phone' => $data['phone'],
            'inv_category_site' => $data['category'],
            'inv_compnay' => $data['company'],
            'inv_date' => '2020-03-03',
            'inv_type' => 'visitor',
            'inv_invitedBy_text' => $data['inv_invitedBy_text'],
        ];

        $this->db->insert('tbl_invitations', $table);
    }


    /**
     * @return mixed
     */
    public function getAllInvitations()
    {
        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date >", date('Y-m-d'));
        $this->db->order_by("inv_id", "DESC");
        return $this->db->get()->result();
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getAllInvitationsByDate($from_date, $to_date, $visited = 0)
    {
        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date >=", $from_date);
        $this->db->where("inv_date <=", $to_date);
        if ($visited == 1) {
            $this->db->where("inv_visited", "1");
        }
        $this->db->order_by("inv_id", "DESC");
        return $this->db->get()->result();
    }


    /***
     * @param $id
     * @return mixed
     */
    public function getAllInvitationsById($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_id", $id);
        $this->db->order_by("inv_id", "DESC");
        return $this->db->get()->row();
    }

    /**
     * @param $status
     * @param $id
     * @return mixed
     */
    public function changeStatus($status, $id)
    {
        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', ['inv_status' => $status]);
    }

    /**
     * @param $status
     * @param $id
     * @return mixed
     */
    public function changeStatusType($status, $id)
    {
        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', ['inv_type' => $status]);
    }

    /**
     * @param $status
     * @param $id
     * @return mixed
     */
    public function changeStatusCall($status, $id)
    {
        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', ['inv_called' => $status]);
    }

    /**
     * @param $status
     * @param $id
     * @return mixed
     */
    public function changeVisitedStatus($status, $id)
    {
        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', ['inv_visited' => $status]);
    }

    /**
     * @param $status
     * @param $id
     * @return mixed
     */
    public function changeStatusFormFilled($status, $id)
    {
        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', ['inv_form_filled' => $status]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCahpterAdminsByChapter($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        // $this->db->from('tbl_chapters_admins');
        //  $this->db->join('tbl_users', 'tbl_chapters_admins.ch_admin_id = tbl_users.userId', 'left');
        $this->db->join('tbl_chapters_admins', 'tbl_chapters_admins.ch_admin_id = tbl_users.userId', 'left');
        $this->db->where('tbl_chapters_admins.ch_chap_id', $id);
        $this->db->where('tbl_users.isDeleted', 0);
        $this->db->order_by('tbl_users.name', "ASC");
        $this->db->group_by('tbl_users.userId');
        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCahpterIdByChapterAdmin($id)
    {
        $this->db->select('ch_chap_id');
        $this->db->from('tbl_chapters_admins');
        $this->db->where('ch_admin_id', $id);
        return $this->db->get()->row();
    }

    /**
     *
     */
    public function getChaptersbyAdminAssigned($admin_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chapters');
        $this->db->join('tbl_chapters_admins', 'tbl_chapters_admins.ch_chap_id = tbl_chapters.chap_id');
        $this->db->where('ch_admin_id', $admin_id);
        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAdmins()
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('chap_id', (int)$this->session->userdata('chapter_id'));
        $this->db->where('isDeleted', 0);
        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @param $chapter
     * @return mixed
     */
    public function getDatesByAmdin($id, $chapter)
    {
        $this->db->select('*');
        $this->db->from('chapter_dates');
        $this->db->join('tbl_chapters_admins', 'tbl_chapters_admins.ch_chap_id = chapter_dates.cahpter_id', 'left');
        // $this->db->where('tbl_chapters_admins.ch_admin_id', $id);
        $this->db->where('tbl_chapters_admins.ch_chap_id', $chapter);
        $this->db->where('chapter_dates.cahpter_date >', date('Y-m-d'));
        $this->db->group_by('chapter_dates.cahpter_date');
        return $this->db->get()->result();
    }

    public function updateInvitationDetail($post)
    {
        $post['invitationId'];

        $data = [
            'inv_type' => $post['type'],
            'inv_called' => $post['isCalled'],
            'inv_status' => $post['confirmation'],
            'inv_form_filled' => $post['formfilled'],
            'inv_visited' => $post['visited'],
            'inv_interview_date' => $post['interview_date'],
            'inv_interviewedBy1' => $post['interviewer1'],
            'inv_interviewedBy2' => $post['interviewer2'],
            'inv_voting_done' => $post['voting'],
            'inv_selection_status' => $post['selection']
        ];

        $this->db->where('inv_id', $post['invitationId']);
        return $this->db->update('tbl_invitations', $data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function addNewCategory($cat_name)
    {
        $this->db->insert("tbl_categories", ['cat_name' => $cat_name]);
        return $this->db->insert_id();
    }

    /**
     * @param $data
     */
    public function adminInvitation($data, $id = "")
    {
        $table = [
            'inv_name' => $data['inv_name'],
            'inv_email' => $data['inv_email'],
            'inv_phone' => $data['inv_phone'],
            'inv_category' => $data['inv_category'],
            'inv_compnay' => $data['inv_company'],
            'inv_date' => $data['inv_date'],
            'inv_invitedBy' => $data['inv_invitedBy'],
            'inv_type' => $data['inv_type'],
            'inv_status' => $data['inv_confirmation'],
            'inv_called' => $data['inv_isCalled'],
            'inv_visited' => $data['visited'],
            'inv_form_filled' => $data['inv_formfilled'],
            'inv_interview_date' => $data['inv_interview_date'],
            'inv_interviewedBy1' => $data['inv_interviewer1'],
            'inv_interviewedBy2' => $data['inv_interviewer2'],
            'inv_voting_done	' => $data['inv_voting'],
            'inv_selection_status' => $data['inv_selection'],
        ];

        if (isset($data['new_category']) && $data['new_category'] != "") {
            $category = $this->addNewCategory($data['new_category']);
            if ($category > 0) {
                $table['inv_category'] = $category;
            }
        }
        if (isset($data['inv_invitedBy_text']) && $data['inv_invitedBy_text'] != "") {

            $table['inv_invitedBy_text'] = $data['inv_invitedBy_text'];
        }

        if ($id) {
            $this->db->where("inv_id", $id);
            return $this->db->update("tbl_invitations", $table);
        } else {
            return $this->db->insert("tbl_invitations", $table);
        }
    }

    public function deleteInvitation($inv_id)
    {
        if ($this->db->delete('tbl_invitations', array('inv_id' => $inv_id))) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getHolidaysList($chapter_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_holidays');
        $this->db->where('chapter_id', $chapter_id);
        $this->db->order_by("holiday_date", "DESC");
        return $this->db->get()->result();
    }

    public function saveHoliday($data)
    {
        $holiday = [
            'name' => $data['name'],
            'holiday_date' => sysToDbDate($data['holiday_date']),
            'chapter_id' => $data['chapter_id'],
        ];

        $this->db->insert('tbl_holidays', $holiday);
        $holiday_id = $this->db->insert_id();

        return $holiday_id;
    }

    public function updateHoliday($post)
    {

        $data = [
            'name' => $post['name'],
            'holiday_date' => sysToDbDate($post['holiday_date']),
            'chapter_id' => $post['chapter_id'],
        ];

        $this->db->where('id', $post['id']);
        return $this->db->update('tbl_holidays', $data);
    }

    public function deleteHoliday($id)
    {
        if ($this->db->delete('tbl_holidays', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getHolidayById($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_holidays');
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }


    /**
     * @return mixed
     */
    public function getFreeweekList($chapter_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_freeweeks');
        $this->db->where('chapter_id', $chapter_id);
        $this->db->order_by("week_date", "DESC");
        return $this->db->get()->result();
    }

    public function saveFreeweek($data)
    {
        $holiday = [
            'name' => $data['name'],
            'week_date' => sysToDbDate($data['week_date']),
            'chapter_id' => $data['chapter_id'],
        ];

        $this->db->insert('tbl_freeweeks', $holiday);
        $holiday_id = $this->db->insert_id();

        return $holiday_id;
    }

    public function updateFreeweek($post)
    {

        $data = [
            'name' => $post['name'],
            'week_date' => sysToDbDate($post['week_date']),
            'chapter_id' => $post['chapter_id'],
        ];

        $this->db->where('id', $post['id']);
        return $this->db->update('tbl_freeweeks', $data);
    }

    public function deleteFreeweek($id)
    {
        if ($this->db->delete('tbl_freeweeks', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getFreeweekById($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_freeweeks');
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    public function adminInvitation_weekly($data, $id = "")
    {
        $table = [
            'inv_name' => $data['inv_name'],
            'inv_email' => $data['inv_email'],
            'inv_phone' => $data['inv_phone'],
            'inv_category' => $data['inv_category'],
            'inv_compnay' => $data['inv_company'],
            'inv_date' => sysToDbDate($data['inv_date']),
            'inv_invitedBy' => $data['inv_invitedBy'],
            'inv_type' => $data['inv_type'],
            'inv_status' => $data['inv_confirmation'],
            'inv_called' => $data['inv_isCalled'],
            'inv_visited' => $data['visited'],
            'inv_form_filled' => $data['inv_formfilled'],
            'inv_interview_date' => $data['inv_interview_date'],
            'inv_interviewedBy1' => $data['inv_interviewer1'],
            'inv_interviewedBy2' => $data['inv_interviewer2'],
            'inv_voting_done	' => $data['inv_voting'],
            'inv_selection_status' => $data['inv_selection'],
        ];


        if (isset($data['new_category']) && $data['new_category'] != "") {
            $category = $this->addNewCategory($data['new_category']);
            if ($category > 0) {
                $table['inv_category'] = $category;
            }
        }
        if (isset($data['inv_invitedBy_text']) && $data['inv_invitedBy_text'] != "") {

            $table['inv_invitedBy_text'] = $data['inv_invitedBy_text'];
        }

        if ($id) {
            $this->db->where("inv_id", $id);
            return $this->db->update("tbl_invitations", $table);
        } else {
            return $this->db->insert("tbl_invitations", $table);
        }
    }


    public function getAllInvitationsByDateWeekly($inv_date, $chapter_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date", $inv_date);
        /* $this->db->where("inv_date >=", $from_date);
         $this->db->where("inv_date <=", $to_date);*/
        $this->db->where("chapter_id", $chapter_id);
        $this->db->order_by("inv_id", "DESC");
        return $this->db->get()->result();
    }

    public function adminInvitation_weeklyUpdate($post, $id)
    {


        $data = [
            'inv_form_filled' => $post['inv_form_filled'],
            'inv_rating' => $post['inv_rating'],
            'inv_comments' => $post['inv_comments'],

        ];
        if (isset($post['visited_check'])) {
            $data['inv_visited'] = "1";
        } else {
            $data['inv_visited'] = "0";
        }

        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', $data);
    }

    public function getAppStatus($chapter_id, $selection_status = 'all', $from_date = 0, $to_date = 0)
    {
        $this->db->select('tbl_invitations.*,uinter1.name as interview1,uinter2.name as interview2,invited_by.name as invited_by_name,tbl_categories.cat_name');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users uinter1', 'tbl_invitations.inv_interviewedBy1 = uinter1.userId', 'left');
        $this->db->join('tbl_users uinter2', 'tbl_invitations.inv_interviewedBy2 = uinter2.userId', 'left');
        $this->db->join('tbl_users invited_by', 'tbl_invitations.inv_invitedBy = invited_by.userId', 'left');
        $this->db->where("chapter_id", $chapter_id);
        $this->db->where("inv_form_filled", "1");

        if ($selection_status != 'all') {
            $this->db->where("inv_selection_status", $selection_status);
        } else {
            //  $this->db->where_in('inv_selection_status', ['0', '1', '5', '6', '7']);
        }
        if ($from_date != 0 && $to_date != 0) {
            $this->db->where("inv_date >=", sysToDbDate($from_date));
            $this->db->where("inv_date <=", sysToDbDate($to_date));
        }


        $this->db->order_by("tbl_invitations.inv_date", "DESC");
        return $this->db->get()->result();
    }

    public function appStatusUpdate($post, $id)
    {


        if (isset($post['docs'])) {
            $checkBox = implode(',', $_POST['docs']);
        } else {
            $checkBox = '';
        }


        $data = [
            'inv_interviewedBy1' => $post['inv_interviewedBy1'],
            'inv_interviewedBy2' => $post['inv_interviewedBy2'],
            'inv_selection_status' => $post['inv_selection_status'],
            'inv_interview_date' => $post['inv_interview_date'],
            'inv_category' => $post['inv_category'],
            'inv_rating' => $post['inv_rating'],
            'documents' => $checkBox,
            'inv_comments' => $post['inv_comments'],
        ];
        if (isset($post['new_category']) && $post['new_category'] != "") {
            $category = $this->addNewCategory($post['new_category']);
            if ($category > 0) {
                $data['inv_category'] = $category;
            }
        }

        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', $data);
    }


    public function add_weekly_fee($data)
    {
        $table = [
            'user_id' => $data['user_id'],
            'amount' => $data['amount'],
            'week_date' => sysToDbDate($data['week_date']),
            'chapter_id' => $data['chapter_id'],
            'description' => $data['description'],
            'created_by' => $this->session->userdata('userId'),
            'created_date' => date('Y-m-d h:i:s'),
        ];
        return $this->db->insert("tbl_weeklyfee", $table);
    }


    public function getAllWeeklyFee($chapter_id, $from_date, $to_date = 0)
    {
        $this->db->select('*');
        $this->db->from('tbl_weeklyfee');
        $this->db->join('tbl_users', 'tbl_weeklyfee.user_id = tbl_users.userId', 'left');
        //$this->db->where("inv_date", $inv_date);
        if ($to_date != 0) {
            $this->db->where("week_date >=", $from_date);
            $this->db->where("week_date <=", $to_date);
        } else {

            $this->db->where("week_date", $from_date);
        }
        $this->db->where("chapter_id", $chapter_id);
        $this->db->order_by("id", "DESC");
        return $this->db->get()->result();
    }

    public function update_weekly_fee($post, $id)
    {


        $data = [
            'user_id' => $post['user_id'],
            'amount' => $post['amount'],
            'week_date' => sysToDbDate($post['week_date']),
            'description' => $post['description'],
            'updated_by' => $this->session->userdata('userId'),
            'updated_date' => date('Y-m-d h:i:s'),

        ];

        $this->db->where('id', $id);
        return $this->db->update('tbl_weeklyfee', $data);
    }

    public function deleteWeekFee($inv_id)
    {
        if ($this->db->delete('tbl_weeklyfee', array('id' => $inv_id))) {
            return true;
        }
        return false;
    }


    public function add_weekly_receipt($data)
    {
        $table = [
            'user_id' => $data['user_id'],
            'amount' => $data['amount'],
            'week_date' => sysToDbDate($data['week_date']),
            'chapter_id' => $data['chapter_id'],
            'description' => $data['description'],
            'created_by' => $this->session->userdata('userId'),
            'created_date' => date('Y-m-d h:i:s'),
        ];
        return $this->db->insert("tbl_weeklyreceipts", $table);
    }


    public function getAllWeeklyReceipts($chapter_id, $from_date, $to_date = 0)
    {
        $this->db->select('*');
        $this->db->from('tbl_weeklyreceipts');
        $this->db->join('tbl_users', 'tbl_weeklyreceipts.user_id = tbl_users.userId', 'left');
        //$this->db->where("inv_date", $inv_date);
        if ($to_date != 0) {
            $this->db->where("week_date >=", $from_date);
            $this->db->where("week_date <=", $to_date);
        } else {

            $this->db->where("week_date", $from_date);
        }
        $this->db->where("chapter_id", $chapter_id);
        $this->db->order_by("id", "DESC");
        return $this->db->get()->result();
    }

    public function update_weekly_receipt($post, $id)
    {


        $data = [
            'user_id' => $post['user_id'],
            'amount' => $post['amount'],
            'week_date' => sysToDbDate($post['week_date']),
            'description' => $post['description'],
            'updated_by' => $this->session->userdata('userId'),
            'updated_date' => date('Y-m-d h:i:s'),

        ];

        $this->db->where('id', $id);
        return $this->db->update('tbl_weeklyreceipts', $data);
    }

    public function deleteWeekReceipt($inv_id)
    {
        if ($this->db->delete('tbl_weeklyreceipts', array('id' => $inv_id))) {
            return true;
        }
        return false;
    }


    public function add_weekly_visitor_fee($data)
    {
        $table = [
            'user_id' => $data['user_id'],
            'amount' => $data['amount'],
            'week_date' => sysToDbDate($data['week_date']),
            'chapter_id' => $data['chapter_id'],
            'description' => $data['description'],
            'created_by' => $this->session->userdata('userId'),
            'created_date' => date('Y-m-d h:i:s'),
        ];
        return $this->db->insert("tbl_weeklyfeevisitors", $table);
    }


    public function getAllWeeklyVisitorFee($chapter_id, $from_date, $to_date = 0)
    {
        $this->db->select('*');
        $this->db->from('tbl_weeklyfeevisitors');
        $this->db->join('tbl_invitations', 'tbl_weeklyfeevisitors.user_id = tbl_invitations.inv_id', 'left');
        //$this->db->where("inv_date", $inv_date);
        if ($to_date != 0) {
            $this->db->where("week_date >=", $from_date);
            $this->db->where("week_date <=", $to_date);
        } else {

            $this->db->where("week_date", $from_date);
        }
        $this->db->where("tbl_weeklyfeevisitors.chapter_id", $chapter_id);
        $this->db->order_by("id", "DESC");
        return $this->db->get()->result();
    }

    public function update_weekly_visitor_fee($post, $id)
    {


        $data = [
            'user_id' => $post['user_id'],
            'amount' => $post['amount'],
            'week_date' => sysToDbDate($post['week_date']),
            'description' => $post['description'],
            'updated_by' => $this->session->userdata('userId'),
            'updated_date' => date('Y-m-d h:i:s'),

        ];

        $this->db->where('id', $id);
        return $this->db->update('tbl_weeklyfeevisitors', $data);
    }

    public function deleteWeekVisitorFee($inv_id)
    {
        if ($this->db->delete('tbl_weeklyfeevisitors', array('id' => $inv_id))) {
            return true;
        }
        return false;
    }

    public function getAllInvitationsVistors($from_date)
    {
        $this->db->select('inv_id,inv_name');
        $this->db->from('tbl_invitations');
        //$this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        //  $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date", $from_date);
        $this->db->where("inv_type", "visitor");
        // $this->db->where("inv_date <=", $to_date);
        //  if ($visited == 1) {
        //      $this->db->where("inv_visited", "1");
        //  }
        $this->db->order_by("inv_id", "DESC");
        return $this->db->get()->result();
    }

    public function add_weekly_visitor_receipt($data)
    {
        $table = [
            'user_id' => $data['user_id'],
            'amount' => $data['amount'],
            'week_date' => sysToDbDate($data['week_date']),
            'chapter_id' => $data['chapter_id'],
            'description' => $data['description'],
            'created_by' => $this->session->userdata('userId'),
            'created_date' => date('Y-m-d h:i:s'),
        ];
        return $this->db->insert("tbl_weeklyreceiptsvisitors", $table);
    }
    public function getAllWeeklyVisitorReceipt($chapter_id, $from_date, $to_date = 0)
    {
        $this->db->select('*');
        $this->db->from('tbl_weeklyreceiptsvisitors');
        $this->db->join('tbl_invitations', 'tbl_weeklyreceiptsvisitors.user_id = tbl_invitations.inv_id', 'left');
        //$this->db->where("inv_date", $inv_date);
        if ($to_date != 0) {
            $this->db->where("week_date >=", $from_date);
            $this->db->where("week_date <=", $to_date);
        } else {

            $this->db->where("week_date", $from_date);
        }
        $this->db->where("tbl_weeklyreceiptsvisitors.chapter_id", $chapter_id);
        $this->db->order_by("id", "DESC");
        return $this->db->get()->result();
    }

    public function update_weekly_visitor_receipt($post, $id)
    {


        $data = [
            'user_id' => $post['user_id'],
            'amount' => $post['amount'],
            'week_date' => sysToDbDate($post['week_date']),
            'description' => $post['description'],
            'updated_by' => $this->session->userdata('userId'),
            'updated_date' => date('Y-m-d h:i:s'),

        ];

        $this->db->where('id', $id);
        return $this->db->update('tbl_weeklyreceiptsvisitors', $data);
    }

    public function deleteWeekVisitorReceipt($inv_id)
    {
        if ($this->db->delete('tbl_weeklyreceiptsvisitors', array('id' => $inv_id))) {
            return true;
        }
        return false;
    }

    function allposts_count($from_date, $to_date, $visited = 0)
    {
        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date >=", $from_date);
        $this->db->where("inv_date <=", $to_date);
        if ($visited == 1) {
            $this->db->where("inv_visited", "1");
        }
        $this->db->order_by("inv_id", "DESC");
        return  $this->db->get()->num_rows();
    }

    function allposts($from_date, $to_date, $visited = 0, $limit, $start, $col, $dir)
    {


        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date >=", $from_date);
        $this->db->where("inv_date <=", $to_date);

        if ($visited == 1) {
            $this->db->where("inv_visited", "1");
        }

        $this->db->limit($limit,$start)->order_by($col,$dir);
        $results = $this->db->get()->result();

        if(count($results) > 0)
        {

            return $results;
        }
        else
        {
            return null;
        }
    }

    function posts_search($from_date, $to_date, $visited = 0,$limit,$start,$search,$col,$dir)
    {

        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date >=", $from_date);
        $this->db->where("inv_date <=", $to_date);
        $this->db->like('inv_id',$search);
        $this->db->or_like('inv_name',$search);
        if ($visited == 1) {
            $this->db->where("inv_visited", "1");
        }
        $this->db->order_by($col,$dir);
        $results = $this->db->get()->result();
        if(count($results)>0)
        {
            return $results;
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($from_date, $to_date, $visited = 0,$search)
    {


        $this->db->select('*');
        $this->db->from('tbl_invitations');
        $this->db->join('tbl_categories', 'tbl_invitations.inv_category = tbl_categories.cat_id', 'left');
        $this->db->join('tbl_users', 'tbl_invitations.inv_invitedBy = tbl_users.userId', 'left');
        $this->db->where("inv_date >=", $from_date);
        $this->db->where("inv_date <=", $to_date);
        $this->db->like('inv_id',$search);
        $this->db->or_like('inv_name',$search);
        if ($visited == 1) {
            $this->db->where("inv_visited", "1");
        }
        return $this->db->get()->num_rows();
    }


}

  