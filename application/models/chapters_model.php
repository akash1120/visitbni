<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chapters_model extends CI_Model
{
    /**
     * @return mixed
     */
    public function getAllCategories()
    {
        $this->db->select('*');
        $this->db->from('tbl_categories');
        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCatName($id)
    {
        $this->db->select('cat_name');
        $this->db->from('tbl_categories');
        $this->db->where('cat_id', $id);
        return $this->db->get()->row();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function adminInfo($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('userId', $id);
        return $this->db->get()->row();
    }

    /**
     * @param $data
     */
    public function saveInfo($data)
    {
        $chapter = [
            'chap_name' => $data['name'],
            'chap_location' => $data['location'],
            'chap_location_map' => $data['location_map'],
            'chap_meeting_fee' => $data['meeting_fee'],
            'event_day' => $data['event_day'],
        ];

        $this->db->insert('tbl_chapters', $chapter);
        $chapter_id = $this->db->insert_id();

        if ($chapter_id) {
            if (count($data['chapter_date']) > 0) {
                foreach ($data['chapter_date'] as $date) {
                    if ($date <> null && !empty($date)) {
                        $dateForm = date('Y-m-d',strtotime($date));
                        $this->db->insert('chapter_dates', ['cahpter_date' => $dateForm, 'cahpter_id' => $chapter_id]);
                    }
                }
            }
        }
        return $chapter_id;
    }

    /**
     * @param $data
     */
    public function updateInfo($data)
    {

        $chapter = [
            'chap_name' => $data['name'],
            'chap_location' => $data['location'],
            'chap_meeting_fee' => $data['meeting_fee'],
            'chap_location_map' => $data['location_map'],
            'event_day' => $data['event_day'],
            'email_template' => $data['email_template'],
        ];

        $this->db->where('chap_id', $data['id']);
        $update = $this->db->update('tbl_chapters', $chapter);

        if ($update) {
            if (count($data['chapter_date']) > 0) {
                $this->db->delete('chapter_dates', array('cahpter_id' => $data['id']));
                foreach ($data['chapter_date'] as $date) {
                    if ($date <> null && !empty($date)) {
                        $dateForm = date('Y-m-d',strtotime($date));
                        $this->db->insert('chapter_dates', ['cahpter_date' => $dateForm, 'cahpter_id' => $data['id']]);
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param $id
     */
    public function deleteChapter($id)
    {

        if ($this->db->delete('tbl_chapters', array('chap_id' => $id))) {
            $this->db->delete('chapter_dates', array('cahpter_id' => $id));
        }
    }

    /**
     * @param $id
     */
    public function deleteChapterAdmin($id)
    {

        if ($this->db->delete('tbl_chapters_admins', array('ch_ad_id' => $id))) {
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function getAllChapters()
    {
        $this->db->select('*');
        $this->db->from('tbl_chapters');
        return $this->db->get()->result();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOneChapter($id)
    {
        $this->db->select('*');
        $this->db->where(['chap_id' => $id]);
        $this->db->from('tbl_chapters');
        return $this->db->get()->row();
    }
    public function getCurrentChapter($id)
    {
        $this->db->select('chap_id,chap_name,chap_meeting_fee,chap_location,chap_location_map,event_day');
        $this->db->where(['chap_id' => $id]);
        $this->db->from('tbl_chapters');
        return $this->db->get()->row();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getChapterDates($id)
    {
        $this->db->select('*');
        $this->db->where(['cahpter_id' => $id]);
        $this->db->from('chapter_dates');
        return $this->db->get()->result();
    }

    /**
     * @param $status
     * @param $id
     * @return mixed
     */
    public function changeStatus($status, $id)
    {
        $this->db->where('inv_id', $id);
        return $this->db->update('tbl_invitations', ['inv_status' => $status]);
    }

    public function getAllChapterAdmins()
    {
        $this->db->select('*');
        $this->db->from('tbl_chapters_admins');
        $this->db->join('tbl_chapters', 'tbl_chapters_admins.ch_chap_id = tbl_chapters.chap_id', 'left');
        $this->db->join('tbl_users', 'tbl_chapters_admins.ch_admin_id = tbl_users.userId', 'left');
        return $this->db->get()->result();
    }

    /**
     * @param $data
     */
    public function saveChapterAdminsInfo($data)
    {

        $chapterAdmin = [
            'ch_admin_id' => $data['user'],
            'ch_chap_id' => $data['chapter']
        ];

        if (!$this->isChapterAdmin($chapterAdmin)) {
            $this->db->insert('tbl_chapters_admins', $chapterAdmin);
            return $chapter_id = $this->db->insert_id();
        }
    }

    /**
     * @param $data
     */
    public function updateChapterAdminsInfo($data)
    {
        $chapterAdmin = [
            'ch_admin_id' => $data['user'],
            'ch_chap_id' => $data['chapter']
        ];

        $this->db->where('ch_ad_id',  $data['chapter_admin_id']);
        return $this->db->update('tbl_chapters_admins', $chapterAdmin);
    }

    /**
     * @param $chapterAdmin
     * @return mixed
     */
    public function isChapterAdmin($chapterAdmin)
    {
        $this->db->select('*');
        $this->db->where($chapterAdmin);
        $this->db->from('tbl_chapters_admins');
        return $this->db->get()->row();

    }

    /**
     * @param $chapterAdmin
     * @return mixed
     */
    public function getChapterAdmin($id)
    {
        $this->db->select('*');
        $this->db->where(['ch_ad_id' => $id]);
        $this->db->from('tbl_chapters_admins');
        return $this->db->get()->row();

    }



}

  